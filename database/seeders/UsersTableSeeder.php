<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'    => 'rizky',
            'email'    => 'rizkypatra5@gmail.com',
            'password'    => bcrypt('28112000')
        ]);
    }
}
