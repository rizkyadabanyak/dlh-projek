<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => "isu daerah",
                'slug' => \Str::slug('isu daerah'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "dlh-jatim",
                'slug' => \Str::slug('dlh-jatim'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "siaran pers",
                'slug' => \Str::slug('siaran pers'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "indeks kualitas lingkungan",
                'slug' => \Str::slug('indeks kualitas lingkungan'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "e-laporan",
                'slug' => \Str::slug('e-laporan'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "ppid dan info terbaru",
                'slug' => \Str::slug('ppid dan info terbaru'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "aksi lingkungan",
                'slug' => \Str::slug('aksi lingkungan'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'name' => "penghargaan dan kemitraan csr",
                'slug' => \Str::slug('penghargaan dan kemitraan csr'),
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],

        ];

        \DB::table('categories')->insert($categories);
    }
}
