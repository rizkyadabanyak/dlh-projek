<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AgendassTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => "1",
                'name' => "Hari Ozon Sedunia : Ozon Untuk Kehidupan",
                'slug' => \Str::slug('Hari Ozon Sedunia : Ozon Untuk Kehidupan'),
                'img' => "uploads/news/1/995428364.jpg",
                'desc' => "Hari Ozon Sedunia ini penting karena merupakan hari yang digunakan untuk menyebarkan kesadaran tentang menipisnya Lapisan Ozon, sekaligus mencari solusi untuk melestarikannya. Salah satu cara terbaik untuk merayakan Hari Ozon Dunia adalah dengan mempelajari lebih lanjut tentang lapisan ozon. Lapisan ozon merupakan bagian dari atmosfer yang memiliki konsentrasi ozon tinggi. Ozon adalah gas yang terbuat dari tiga atom oksigen O3. Tergantung di mana lapisan ozon berada, hal itu dapat membahayakan kehidupan atau melindungi kehidupan di Bumi. Sebagian besar ozon tetap berada di dalam stratosfer di mana ia bertindak sebagai perisai, melindungi permukaan bumi dari radiasi ultraviolet matahari yang berbahaya. Jika perisai ini dilemahkan, kita semua akan lebih rentan terhadap gangguan sistem kekebalan, katarak, dan kanker kulit. Ozon bisa menjadi polutan berbahaya yang menyebabkan kerusakan pada tumbuhan dan jaringan paru-paru jika lebih dekat ke Bumi daripada troposfer.yang merupakan lapisan atmosfer dari permukaan sampai kira-kira sepuluh km. Ini menunjukkan mengapa lapisan ozon sangat penting, dan sangat penting untuk mengelolanya secara efektif.",
                'desc_banner' => "Hari Ozon Sedunia ini penting karena merupakan hari yang digunakan untuk menyebarkan kesadaran tentang menipisnya Lapisan Ozon, sekaligus mencari solusi untuk melestarikannya. Salah satu cara terbaik untuk merayakan Hari Ozon Dunia adalah dengan mempelajari lebih lanjut tentang lapisan ozon. Lapisan ozon merupakan bagian dari atmosfer yang memiliki konsentrasi ozon tinggi. Ozon adalah gas yang terbuat dari tiga atom oksigen O3. Tergantung di mana lapisan ozon berada, hal itu dapat membahayakan kehidupan atau melindungi kehidupan di Bumi. Sebagian besar ozon tetap berada di dalam stratosfer di mana ia bertindak sebagai perisai, melindungi permukaan bumi dari radiasi ultraviolet matahari yang berbahaya. Jika perisai ini dilemahkan, kita semua akan lebih rentan terhadap gangguan sistem kekebalan, katarak, dan kanker kulit. Ozon bisa menjadi polutan berbahaya yang menyebabkan kerusakan pada tumbuhan dan jaringan paru-paru jika lebih dekat ke Bumi daripada troposfer.yang merupakan lapisan atmosfer dari permukaan sampai kira-kira sepuluh km. Ini menunjukkan mengapa lapisan ozon sangat penting, dan sangat penting untuk mengelolanya secara efektif.",
                'active_flag' => "1",
                'time' => "11:12",
                'date_start' => "2021-12-11 11:12:09",
                'date_end' => "2021-12-11 11:12:09",
                'location' => "surabaya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('agendas')->insert($data);
    }
}
