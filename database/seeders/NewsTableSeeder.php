<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => "1",
                'category_id' => "2",
                'name' => "Hari Ozon Sedunia : Ozon Untuk Kehidupan",
                'slug' => \Str::slug('Hari Ozon Sedunia : Ozon Untuk Kehidupan'),
                'img' => "uploads/news/1/995428364.jpg",
                'desc' => "Hari Ozon Sedunia ini penting karena merupakan hari yang digunakan untuk menyebarkan kesadaran tentang menipisnya Lapisan Ozon, sekaligus mencari solusi untuk melestarikannya. Salah satu cara terbaik untuk merayakan Hari Ozon Dunia adalah dengan mempelajari lebih lanjut tentang lapisan ozon. Lapisan ozon merupakan bagian dari atmosfer yang memiliki konsentrasi ozon tinggi. Ozon adalah gas yang terbuat dari tiga atom oksigen O3. Tergantung di mana lapisan ozon berada, hal itu dapat membahayakan kehidupan atau melindungi kehidupan di Bumi. Sebagian besar ozon tetap berada di dalam stratosfer di mana ia bertindak sebagai perisai, melindungi permukaan bumi dari radiasi ultraviolet matahari yang berbahaya. Jika perisai ini dilemahkan, kita semua akan lebih rentan terhadap gangguan sistem kekebalan, katarak, dan kanker kulit. Ozon bisa menjadi polutan berbahaya yang menyebabkan kerusakan pada tumbuhan dan jaringan paru-paru jika lebih dekat ke Bumi daripada troposfer.yang merupakan lapisan atmosfer dari permukaan sampai kira-kira sepuluh km. Ini menunjukkan mengapa lapisan ozon sangat penting, dan sangat penting untuk mengelolanya secara efektif.",
                'desc_banner' => "Hari Ozon Sedunia ini penting karena merupakan hari yang digunakan untuk menyebarkan kesadaran tentang menipisnya Lapisan Ozon, sekaligus mencari solusi untuk melestarikannya. Salah satu cara terbaik untuk merayakan Hari Ozon Dunia adalah dengan mempelajari lebih lanjut tentang lapisan ozon. Lapisan ozon merupakan bagian dari atmosfer yang memiliki konsentrasi ozon tinggi. Ozon adalah gas yang terbuat dari tiga atom oksigen O3. Tergantung di mana lapisan ozon berada, hal itu dapat membahayakan kehidupan atau melindungi kehidupan di Bumi. Sebagian besar ozon tetap berada di dalam stratosfer di mana ia bertindak sebagai perisai, melindungi permukaan bumi dari radiasi ultraviolet matahari yang berbahaya. Jika perisai ini dilemahkan, kita semua akan lebih rentan terhadap gangguan sistem kekebalan, katarak, dan kanker kulit. Ozon bisa menjadi polutan berbahaya yang menyebabkan kerusakan pada tumbuhan dan jaringan paru-paru jika lebih dekat ke Bumi daripada troposfer.yang merupakan lapisan atmosfer dari permukaan sampai kira-kira sepuluh km. Ini menunjukkan mengapa lapisan ozon sangat penting, dan sangat penting untuk mengelolanya secara efektif.",
                'status' => "active",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'user_id' => "1",
                'category_id' => "2",
                'name' => "Waktunya Kembali ke Alam : Makna Peringatan Hari Lingkungan Hidup Se Dunia 2020",
                'slug' => \Str::slug('Waktunya Kembali ke Alam : Makna Peringatan Hari Lingkungan Hidup Se Dunia 2020'),
                'img' => "uploads/news/1/91HRLH.jpg",
                'desc' => "Hari ini, 5 Juni 2020. Lonceng besar pengingat kita untuk bersama melakukan gerakan Pelestarian Lingkungan Hidup. Hari ini merupakan  Peringatan Hari Lingkungan Hidup Sedunia Tema dari peringatan ini adalah Time for Nature Mari kita lihat lingkup kecil disekeliling kita : 1. Tanaman yang sudah kita tanam, sudahkah cukup untuk memproduksi oksigen untuk seisi rumah. 2. Bagaimana sampah rumah tangga, sudahkah kita lengkapi. Jangan kotori sungai sebagai sumber kehidupan kita. 3. Bentuklah Keanekaragaman hayati sekeleliling kita. Kolam ikan salah satu contohnya. 4. Bangun Bank Sampah sesederhana apapun untuk memulai mengelola sampah keringmu. Makanan yang kita makan, udara yang kita hirup, air yang kita minum, dan iklim yang membuat planet kita layak huni semuanya berasal dari Alam. Sudah waktunya untuk merawat alam lebih baik lagi bagi manusia dan planet ini. Peringatan Hari Lingkungan Hidup Se Dunia 2020 kali ini dipusatkan di Colombia dengan tema  dan tagline Time for Nature. Indonesia kaya akan ekosistem berlimpah, rumah bagi 720 spesies mamalia, 1.605 sp burung, 409 sp amfibia, 723 sp reptile, 1.900 sp kupu-kupu, 197.964 sp invertebrata, 3.982 sp vertebrata, 151.847 sp serangga dan 30.000 sp hymenoptera. Menghilangkan salah satu komponen dari jaringan keanekaragaman hayati akan menimbulkan dampak negatif bagi kehidupan manusia. Timbulnya bencana pandemik COVID-19 menunjukkan bahwa jika kita merusak keanekaragaman hayati, maka kita juga merusak sistem pendukung kehidupan manusia. Semoga kita bisa bijak terhadap alam lingkungan ini agar alam pun bisa memberikan segala apa yang kita butuhkan (r@ss).",
                'desc_banner' => "Hari ini, 5 Juni 2020. Lonceng besar pengingat kita untuk bersama melakukan gerakan Pelestarian Lingkungan Hidup. Hari ini merupakan Peringatan Hari Lingkungan Hidup Sedunia Tema dari peringatan ini adalah Time for Nature Mari kita lihat lingkup kecil disekeliling kita : 1. Tanaman yang sudah kita tanam, sudahkah cukup untuk memproduksi oksigen untuk seisi rumah. 2. Bagaimana sampah rumah tangga, sudahkah kita lengkapi. Jangan kotori sungai sebagai sumber kehidupan kita. 3. Bentuklah Keanekaragaman hayati sekeleliling kita. Kolam ikan salah satu contohnya. 4. Bangun Bank Sampah sesederhana apapun untuk memulai mengelola sampah keringmu. Makanan yang kita makan, udara yang kita hirup, air yang kita minum, dan iklim yang membuat planet kita layak huni semuanya berasal dari Alam. Sudah waktunya untuk merawat alam lebih baik lagi bagi manusia dan planet ini. Peringatan Hari Lingkungan Hidup Se Dunia 2020 kali ini dipusatkan di Colombia dengan tema dan tagline Time for Nature. Indonesia kaya akan ekosistem berlimpah, rumah bagi 720 spesies mamalia, 1.605 sp burung, 409 sp amfibia, 723 sp reptile, 1.900 sp kupu-kupu, 197.964 sp invertebrata, 3.982 sp vertebrata, 151.847 sp serangga dan 30.000 sp hymenoptera. Menghilangkan salah satu komponen dari jaringan keanekaragaman hayati akan menimbulkan dampak negatif bagi kehidupan manusia. Timbulnya bencana pandemik COVID-19 menunjukkan bahwa jika kita merusak keanekaragaman hayati, maka kita juga merusak sistem pendukung kehidupan manusia. Semoga kita bisa bijak terhadap alam lingkungan ini agar alam pun bisa memberikan segala apa yang kita butuhkan (r@ss).",
                'status' => "active",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'user_id' => "1",
                'category_id' => "2",
                'name' => "Waktunya Kembali ke Alam : Makna Peringatan Hari Lingkungan Hidup Se Dunia 2020a",
                'slug' => \Str::slug('Waktunya Kembali ke Alam : Makna Peringatan Hari Lingkungan Hidup Se Dunia 2020a'),
                'img' => "uploads/news/1/91HRLH.jpg",
                'desc' => "Hari ini, 5 Juni 2020. Lonceng besar pengingat kita untuk bersama melakukan gerakan Pelestarian Lingkungan Hidup. Hari ini merupakan  Peringatan Hari Lingkungan Hidup Sedunia Tema dari peringatan ini adalah Time for Nature Mari kita lihat lingkup kecil disekeliling kita : 1. Tanaman yang sudah kita tanam, sudahkah cukup untuk memproduksi oksigen untuk seisi rumah. 2. Bagaimana sampah rumah tangga, sudahkah kita lengkapi. Jangan kotori sungai sebagai sumber kehidupan kita. 3. Bentuklah Keanekaragaman hayati sekeleliling kita. Kolam ikan salah satu contohnya. 4. Bangun Bank Sampah sesederhana apapun untuk memulai mengelola sampah keringmu. Makanan yang kita makan, udara yang kita hirup, air yang kita minum, dan iklim yang membuat planet kita layak huni semuanya berasal dari Alam. Sudah waktunya untuk merawat alam lebih baik lagi bagi manusia dan planet ini. Peringatan Hari Lingkungan Hidup Se Dunia 2020 kali ini dipusatkan di Colombia dengan tema  dan tagline Time for Nature. Indonesia kaya akan ekosistem berlimpah, rumah bagi 720 spesies mamalia, 1.605 sp burung, 409 sp amfibia, 723 sp reptile, 1.900 sp kupu-kupu, 197.964 sp invertebrata, 3.982 sp vertebrata, 151.847 sp serangga dan 30.000 sp hymenoptera. Menghilangkan salah satu komponen dari jaringan keanekaragaman hayati akan menimbulkan dampak negatif bagi kehidupan manusia. Timbulnya bencana pandemik COVID-19 menunjukkan bahwa jika kita merusak keanekaragaman hayati, maka kita juga merusak sistem pendukung kehidupan manusia. Semoga kita bisa bijak terhadap alam lingkungan ini agar alam pun bisa memberikan segala apa yang kita butuhkan (r@ss).",
                'desc_banner' => "Hari ini, 5 Juni 2020. Lonceng besar pengingat kita untuk bersama melakukan gerakan Pelestarian Lingkungan Hidup. Hari ini merupakan Peringatan Hari Lingkungan Hidup Sedunia Tema dari peringatan ini adalah Time for Nature Mari kita lihat lingkup kecil disekeliling kita : 1. Tanaman yang sudah kita tanam, sudahkah cukup untuk memproduksi oksigen untuk seisi rumah. 2. Bagaimana sampah rumah tangga, sudahkah kita lengkapi. Jangan kotori sungai sebagai sumber kehidupan kita. 3. Bentuklah Keanekaragaman hayati sekeleliling kita. Kolam ikan salah satu contohnya. 4. Bangun Bank Sampah sesederhana apapun untuk memulai mengelola sampah keringmu. Makanan yang kita makan, udara yang kita hirup, air yang kita minum, dan iklim yang membuat planet kita layak huni semuanya berasal dari Alam. Sudah waktunya untuk merawat alam lebih baik lagi bagi manusia dan planet ini. Peringatan Hari Lingkungan Hidup Se Dunia 2020 kali ini dipusatkan di Colombia dengan tema dan tagline Time for Nature. Indonesia kaya akan ekosistem berlimpah, rumah bagi 720 spesies mamalia, 1.605 sp burung, 409 sp amfibia, 723 sp reptile, 1.900 sp kupu-kupu, 197.964 sp invertebrata, 3.982 sp vertebrata, 151.847 sp serangga dan 30.000 sp hymenoptera. Menghilangkan salah satu komponen dari jaringan keanekaragaman hayati akan menimbulkan dampak negatif bagi kehidupan manusia. Timbulnya bencana pandemik COVID-19 menunjukkan bahwa jika kita merusak keanekaragaman hayati, maka kita juga merusak sistem pendukung kehidupan manusia. Semoga kita bisa bijak terhadap alam lingkungan ini agar alam pun bisa memberikan segala apa yang kita butuhkan (r@ss).",
                'status' => "active",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('news')->insert($data);
    }
}
