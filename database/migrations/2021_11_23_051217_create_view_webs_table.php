<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewWebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('view_webs', function (Blueprint $table) {
            $table->id();
//            $table->foreignId('news_id')->constrained('news');
            $table->string('ip');
            $table->string('browser');
            $table->string('country');
            $table->string('path');
            $table->string('city');
            $table->string('so');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_webs');
    }
}
