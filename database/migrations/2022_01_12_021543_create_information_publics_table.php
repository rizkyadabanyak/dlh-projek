<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationPublicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_publics', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('ringkasan');
            $table->string('pejabat');
            $table->string('pj');
            $table->string('waktudantempat');
            $table->string('bentuk');
            $table->string('jangka_waktu');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_publics');
    }
}
