<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('client_name');
            $table->string('client_email');
            $table->string('client_phone');
            $table->dateTime('client_date_of_birth');
            $table->string('client_gender');
            $table->string('client_location');
            $table->string('client_reason');
            $table->string('client_impact');
            $table->string('client_complaint');
            $table->string('client_file');
            $table->string('client_classification');
            $table->string('client_solution');
            $table->enum('status',['0','1','2'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
