<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\InformationPPIDController;
use App\Models\Agenda;
use App\Models\AgendaComment;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\CommentNews;
use App\Models\Complaint;
use App\Models\Galleries;
use App\Models\InformationPublic;
use App\Models\Level;
use App\Models\NewGalleries;
use App\Models\News;
use App\Models\Pariwara;
use App\Models\PermohonanPPID;
use App\Models\Profil;
use App\Models\Regulation;
use App\Models\relatedWebsites;
use App\Models\Report;
use App\Models\Structure;
use App\Models\Tupoksi;
use App\Models\VidioProfile;
use App\Models\View;
use App\Models\ViewWeb;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // Mendapatkan jenis web browser pengunjung

    function url_origin( $s, $use_forwarded_host = false )
    {
        $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
        $sp       = strtolower( $s['SERVER_PROTOCOL'] );
        $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
        $port     = $s['SERVER_PORT'];
        $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
        $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
        $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    function full_url( $s, $use_forwarded_host = false )
    {
        return $this->url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
    }


    public function __construct()
    {
        $browser = $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);
//        get path url
        $absolute_url = $this->full_url($_SERVER);
//        echo $absolute_url;

        $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$this->getRealIpAddr());


        $data = new ViewWeb();

        $data->ip = $xml->geoplugin_request;
        $data->browser	= $browser;
        $data->country = $xml->geoplugin_countryName;
        $data->path = $absolute_url;
        $data->city = $xml->geoplugin_city;
        $data->so = $this->getOS();
        $data->save();

    }
    public function index()
    {
        //get path url
//        $absolute_url = $this->full_url($_SERVER);
//        echo $absolute_url;
//        return;


//        echo $this->ip_info("110.138.226.242", "Country");
//        echo $this->ip_info("110.138.226.242", "Country Code"); // US
//        echo $this->ip_info("110.138.226.242", "State"); // California
//        echo $this->ip_info("110.138.226.242", "City"); // Menlo Park
//        echo $this->ip_info("110.138.226.242", "Address"); // Menlo Park, California, United States
//
//        return;

//        $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$this->getRealIpAddr());
//        echo $xml->geoplugin_countryName ;
//
//
//        echo "<pre>";
//        foreach ($xml as $key => $value)
//        {
//            echo $key , "= " , $value ,  " \n" ;
//        }
//        echo "</pre>";
//        echo "Operating system is: " . $this->getOS() . "<br />";
//        echo "Understood from: " . $_SERVER["HTTP_USER_AGENT"];
//
//
//        dd($this->get_browser_name($_SERVER['HTTP_USER_AGENT']));
//
//        return;

        $news = News::orderBy('created_at', 'DESC')->whereStatus('active')->limit(3)->get();
        $categories = Category::whereActiveFlag('1')->get();
        $pariwaras = Pariwara::orderBy('created_at','DESC')->whereActiveFlag('1')->limit(4)->get();
        $agendas  = Agenda::orderBy('created_at','DESC')->whereActiveFlag('1')->limit(4)->get();
        $newGalleries  = NewGalleries::orderBy('created_at','DESC')->whereActiveFlag('1')->limit(4)->get();
        $relatedWebsites  = relatedWebsites::whereActiveFlag('1')->orderBy('created_at','DESC')->whereActiveFlag('1')->limit(4)->get();
        $carouselsFirst = Carousel::whereActiveFlag('1')->orderBy('created_at', 'DESC')->first();
        $video = VidioProfile::first();
        $carousels = Carousel::take(3)->orderBy('created_at', 'DESC')->get();

        view()->share([
            'news' => $news,
            'categories' => $categories,
            'pariwaras' => $pariwaras,
            'agendas' => $agendas,
            'newGalleries' => $newGalleries,
            'relatedWebsites' => $relatedWebsites,
            'carouselsFirst' => $carouselsFirst,
            'carousels' => $carousels,
            'video' => $video
        ]);
        return view('landing.content.index');
    }

    public function detail($slug){

        $cek = 'new';
        $data = News::whereSlug($slug)->first();
        $news = News::orderBy('created_at', 'DESC')->limit(3)->get();
        $comments = CommentNews::whereNewsId($data->id)->orderBy('created_at', 'DESC')->get();

        view()->share([
            'data' => $data,
            'news' =>$news,
            'cek' => $cek,
            'comments' => $comments
        ]);

        return view('landing.content.detail');
    }

    public function detailAgenda($slug){
        $cek = 'agenda';
        $data = Agenda::whereSlug($slug)->first();
        $news = News::orderBy('created_at', 'DESC')->limit(3)->get();
        $comments = AgendaComment::whereAgendaId($data->id)->orderBy('created_at', 'DESC')->get();

        view()->share([
            'data' => $data,
            'news' =>$news,
            'cek' => $cek,
            'comments' => $comments
        ]);

        return view('landing.content.detail');
    }

    public function detailBeritaFoto($slug){

        $cek = 'foto';
        $news = News::orderBy('created_at', 'DESC')->limit(3)->get();
        $data = NewGalleries::whereSlug($slug)->first();
        $comments = AgendaComment::whereAgendaId($data->id)->orderBy('created_at', 'DESC')->get();
        $agendas = Agenda::orderBy('created_at', 'DESC')->limit(3)->get();

        $galleries = Galleries::whereNewGalleriesId($data->id)->get();

        view()->share([
            'data' => $data,
            'news' =>$news,
            'agendas' => $agendas,
            'cek' => $cek,
            'comments' => $comments,
            'galleries' => $galleries
        ]);

        return view('landing.content.detailFoto');
    }

    public function searchAction(Request $request){
//        dd($request->search);
        $data = News::where('name','like',"%".$request->search."%")->get();

        view()->share([
            'news' => $data
        ]);

        return view('landing.content.search');
    }

    public function searchActionPPID(Request $request){
//        dd($request->search);
        $data = News::where('name','like',"%".$request->search."%")->wherePpid('1')->get();

        view()->share([
            'news' => $data
        ]);

        return view('landing.ppid.search');
    }
    public function profile(Request $request){
//        dd($request->search);
        $data = Profil::first();

        view()->share([
           'data' => $data
        ]);

        return view('landing.content.profile');
    }
    public function tupoksi(Request $request){
//        dd($request->search);
        $data = Tupoksi::first();

        view()->share([
            'data' => $data
        ]);

        return view('landing.content.tupoksi');
    }
    public function structure(Request $request){
        $data = Structure::first();

        view()->share([
            'data' => $data
        ]);

        return view('landing.content.structure');
    }
    public function regulations(Request $request){
//        dd($request->search);
        $data = Regulation::orderBy('created_at', 'DESC')->whereActiveFlag('1')->get();
        $levels = Level::all();

        view()->share([
            'data' => $data,
            'levels' => $levels
        ]);

        return view('landing.content.regulation');
    }
    public function regulationsFilter(Request $request,$id){
//        dd($request->search);
        $data = Regulation::whereLevelId($id)->orderBy('created_at', 'DESC')->get();
        $levels = Level::all();

        view()->share([
            'data' => $data,
            'levels' => $levels
        ]);

        return view('landing.content.regulation');
    }
    public function downloads(Request $request)
    {
//        dd($request->search);
        $data = Report::all();

        view()->share([
            'data' => $data
        ]);
        return view('landing.content.download');
    }

    public function landingPengaduan(){
        $masuk = Complaint::count();
        $ditindaklanjuti = Complaint::whereStatus('1')->count();
        $selesai = Complaint::whereStatus('2')->count();

        view()->share([
            'masuk'  => $masuk,
            'ditindaklanjuti' => $ditindaklanjuti,
            'selesai' =>$selesai
        ]);
//        dd($masuk);
        return view('landing.content.landingPengaduan');
    }
    public function pengaduan(Request $request)
    {
//        dd($request->search);
//        $data = Regulation::first();
//
//        view()->share([
//            'data' => $data
//        ]);
        return view('landing.content.pengaduan');
    }

    public function pengaduanAction(Request $request){
        $validates = [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'ktp' => 'required|max:10000|mimes:pdf,jpg,jpeg,png',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'location' => 'required',
            'reason' => 'required',
            'impact' => 'required',
            'complaint' => 'required',
            'classification' => 'required',
            'solution' => 'required',
            'file' => 'required|max:10000|mimes:pdf,jpg,jpeg,png' //a required, max 10000kb, doc or docx file and file img
        ];

        $request->validate($validates);
//        dd($request);

        $data = new Complaint();

        $data->client_name = $request->name;
        $data->client_email = $request->email;
        $data->client_phone = $request->phone;
        $data->client_date_of_birth = $request->date_of_birth;
        $data->client_gender = $request->gender;
        $data->client_location = $request->location;
        $data->client_reason = $request->reason;
        $data->client_impact = $request->impact;
        $data->client_complaint = $request->complaint;
        $data->client_classification = $request->classification;
        $data->client_solution = $request->solution;


        if ($request->file('ktp')){
            $data->ktp = $this->upload($request->file('ktp'));
        }else{
            $data->ktp = $data->ktp ;
        }
        if ($request->file('file')){
            $data->client_file = $this->upload($request->file('ktp'));
        }else{
            $data->client_file = $data->img ;
        }

        $code = 'AD'.rand(999999999,1);

        $data->code = $code;

        $data->save();

        return redirect()->back()-> withSuccess('Berhasil Mengirim Aduan. Berikut Merupakan Code Aduan : '.$code);

    }
    public function pengaduanActionCek(Request $request){
        $data = Complaint::whereCode($request->code)->first();
        if ($data){
            if ($data->status == '0'){
                return redirect()->back()->withInfo('Pengajuan masih belum di tindak');
            }elseif ($data->status == '1'){
                return redirect()->back()->withWarning('Pengajuan Masih Proses Ditindaklanjuti');
            }else{
                return redirect()->back()->withSuccess('Pengajuan Sudah Selesai Ditindaklanjuti');
            }
        }else{
            return redirect()->back()->withDanger('Code Tidak Valid');

        }
    }

    public function agendaSelectDate ($date){

        $agendas  = Agenda::where('date_start', '=' , $date)->get();

        \view()->share([
            'agendas' => $agendas,
            'date' => $date
        ]);
        return view('landing.content.agendaSelectDate');
    }

    public function moreNew(Request $request){
//        dd($request->search);
        $data = News::orderBy('created_at','DESC')->paginate(5);

        view()->share([
            'news' => $data
        ]);

        return view('landing.content.moreNew');
    }

    public function commentNew ($id, $cek, Request $request){

        if ($cek == 'new'){
            $data = new CommentNews();
            $data->news_id = $id;
        }
        if ($cek == 'agenda'){
            $data = new AgendaComment();
            $data->agenda_id = $id;
        }


        $data->user_name = $request->name;
        $data->user_email = $request->email;
        $data->user_comment = $request->comment;

        $data->save();

        return redirect()->back();
    }

    public function ppid(){
        $data = News::wherePpid('1')->orderBy('created_at','DESC')->paginate(5);
        $informations = InformationPublic::all();

        view()->share([
            'news' => $data,
            'informations' => $informations
        ]);

        return view('landing.ppid.ppid');
    }

    public function ppidPermohonan (){
        return view('landing.ppid.permohonan');
    }
    public function ppidPermohonanAction (Request $request){

        $data = new PermohonanPPID();

        $data->client_name = $request->name;
        $data->client_email = $request->email;
        $data->client_phone = $request->phone;
        $data->client_date_of_birth = $request->date_of_birth;
        $data->client_detail_information = $request->detail;
        $data->client_target_information = $request->target;

        if ($request->memperoleh == null|| $request->mendapatkan == null){
            return redirect()->back();
        }

        $tmp_memperoleh = null;
        foreach ($request->memperoleh as $memperoleh){

            if ($tmp_memperoleh == null){
                $tmp_memperoleh = $memperoleh;

            }else{
                $tmp_memperoleh = $tmp_memperoleh.','.$memperoleh;
            }
        }

        $tmp_mendapatkan = null;
        foreach ($request->mendapatkan as $mendapatkan){

            if ($tmp_mendapatkan == null){
                $tmp_mendapatkan = $mendapatkan;

            }else{
                $tmp_mendapatkan = $tmp_mendapatkan.','.$mendapatkan;
            }
        }
        $data->client_memperoleh = $tmp_memperoleh;
        $data->client_mendapatkan = $tmp_mendapatkan;

        $data->save();

        return redirect()->back();

    }

    public function categorySelect ($slug){

        $category = Category::whereSlug($slug)->first();
        $data = News::whereCategoryId($category->id)->orderBy('created_at','DESC')->paginate(5);

        view()->share([
            'news' => $data
        ]);

        return view('landing.content.moreNew');

        return view('landing.ppid.moreNew');
    }

}
