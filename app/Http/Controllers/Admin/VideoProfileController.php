<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pariwara;
use App\Models\VidioProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = VidioProfile::first();

        view()->share([
           'data' => $data
        ]);
        return view('admin.content.videoProfile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.videoProfile.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'video' => 'required|mimes:mp4,mov'
        ];

        $request->validate($validates);

        $data = new VidioProfile();

        $data->name = $request->name;
        if ($request->file('video')) {
            $file = $request->file('video');
            $name = rand(999999999, 1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name . '.' . $extension;
            $imgDB = 'uploads/video/profile/' . $newName;
            $request->video->move(public_path('uploads/video/profile/'), $newName);
            $data->src = $imgDB;
        } else {
            $data->img = $data->img;
        }
        $data->save();

        return redirect()->route('admin.auth.videoProfile.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
