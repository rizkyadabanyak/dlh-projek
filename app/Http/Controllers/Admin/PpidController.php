<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class PpidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = News::wherePpid('1')->get();

//        dd($data);
            return DataTables::of($data)
                ->addColumn('user', function($data){
                    $a = $data->user->name;
                    return $a;
                })->addColumn('img', function($data){
                    $a = '<img src="'.asset($data->img).'">' ;
                    return $a;
                })->addColumn('desc', function($data){
                    $a = \Str::limit($data->desc_banner, 70, $end='...') ;
                    return $a;
                })->addColumn('status', function($data){
                    if ($data->status == 'active'){
                        $a = '<div class="badge badge-success">'.$data->status.'</div>';

                    }else{
                        $a = '<div class="badge badge-danger">'.$data->status.'</div>';

                    }
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="news/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.auth.newsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['img','desc','user','status','action'])
                ->make(true);
        }
        return view('admin.content.ppid.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $data = null;

        view()->share([
            'categories' => $categories,
            'data' => $data
        ]);
        return view('admin.content.ppid.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'category_id' => 'required',
            'name' => 'required|unique:news,name',
            'desc' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $data = new News();

        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/pppid/'.$request->category_id.'/'.$newName;

            $request->image->move(public_path('uploads/pppid/'.$request->category_id.'/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->desc_banner = strip_tags($request->desc);
        $data->ppid = '1';

        $data->save();

        return redirect()->route('admin.auth.ppid.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = News::find($id);
        $categories = Category::all();

        view()->share([
            'categories' => $categories,
            'data' => $data
        ]);
        return view('admin.content.ppid.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = News::find($id);

        if ($request->name == $data->name){
            $validates = [
                'category_id' => 'required',
                'desc' => 'required',
            ];
        }else{
            $validates = [
                'category_id' => 'required',
                'name' => 'required|unique:news,name',
                'desc' => 'required',
            ];
        }


        $request->validate($validates);



        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/news/'.$request->category_id.'/'.$newName;

            $request->image->move(public_path('uploads/news/'.$request->category_id.'/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->desc_banner = strip_tags($request->desc);
        $data->ppid = '1';


        $data->save();

        return redirect()->route('admin.auth.ppid.index')->withWarning('Succcess update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = News::find($id);

        $data->status = 'non-active';
        $data->save();

        return redirect()->route('admin.auth.pppid.index')->withDanger('Succcess non-active data');;
    }
}
