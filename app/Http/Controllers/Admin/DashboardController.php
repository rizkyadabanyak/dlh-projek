<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NewGalleries;
use App\Models\News;
use App\Models\User;
use App\Models\ViewWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DashboardController extends Controller
{
    public function index(){

        $countUser = User::whereActiveFlag('1')->count();
        $countNew = News::whereStatus('active')->count();
        $news = News::orderBy('created_at', 'DESC')->limit(5)->get();
        $visited = ViewWeb::count();
        view()->share([
           'countUser' => $countUser,
            'countNew' => $countNew,
            'news' => $news,
            'visited' => $visited
        ]);
        return view('admin.content.dashboard.index');
    }

    public function logClients(Request $request){
//        $data = ViewWeb::first();
//        $dt = Carbon::parse($data->created_at);
//
//        dd($dt);
        if($request->ajax()) {
            $data = ViewWeb::all();
            return DataTables::of($data)
                ->addColumn('date', function($data){
                    $dt = Carbon::parse($data->created_at);
                    $a = $dt->toFormattedDateString();
                    return $a;
                })->addColumn('time', function($data){
                    $dt = Carbon::parse($data->created_at);
                    $a = $dt->toTimeString();
                    return $a;
                })
                ->rawColumns(['date','time'])
                ->make(true);
        }
        return view('admin.content.logClients.index');

    }
}
