<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use App\Models\Complaint;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Complaint::orderBy('created_at','DESC')->get();
            return DataTables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 0){
                        $a = '<div class="badge badge-danger">Belum di tindak</div>';

                    }else if ($data->status == 1){
                        $a = '<div class="badge badge-warning">Ditindak</div>';
                    }else{
                        $a = '<div class="badge badge-success">Selesai</div>';
                    }
                    return $a;
                })->addColumn('action', function($data){
                    $button = '<a  href="'.route('admin.auth.complaint.show',$data->id).'" class="btn btn-info">Show</a>';
                    return $button;
                })
                ->rawColumns(['status','action'])
                ->make(true);
        }
        return view('admin.content.pengaduan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Complaint::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.pengaduan.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function tindaklanjut($id){
        $data = Complaint::find($id);

        $data->status = '1';

        $data->save();

        return redirect()->back()->withSuccess('Proses Tindaklanjut');
    }

    public function selesai($id){
        $data = Complaint::find($id);

        $data->status = '2';

        $data->save();

        return redirect()->back()->withSuccess('aduan telah selesai');
    }
}
