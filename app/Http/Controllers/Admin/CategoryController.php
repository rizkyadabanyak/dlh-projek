<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Category::whereActiveFlag('1')->get();

            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="categories/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.auth.categoriesDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.categories.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
          'name' => 'required|unique:categories,name'
        ];
        $request->validate($validates);

        $data = new Category();
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);

        $data->save();

        return redirect()->route('admin.auth.categories.index')->withSuccess('Succcess create category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::find($id);
        view()->share([
           'data' => $data
        ]);
        return view('admin.content.categories.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $validates = [
          'name' => 'required|unique:categories,name'
        ];
        $request->validate($validates);

        $data = Category::find($id);
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);

        $data->save();

        return redirect()->route('admin.auth.categories.index')->withWarning('Succcess update category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::find($id);

        $data->active_flag = '0';
        $data->save();

        return redirect()->route('admin.auth.categories.index')->withDanger('Succcess delete category');;
    }
}
