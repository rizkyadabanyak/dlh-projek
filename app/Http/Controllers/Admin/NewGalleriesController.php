<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Galleries;
use App\Models\NewGalleries;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class NewGalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectDelete(Request $request) {
        $data = $request->images;

        foreach ($data as $item){
            Galleries::where('id','=',$item)->delete();
        }

        return redirect()->back()->with('success','success delete images');
    }
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = NewGalleries::whereActiveFlag('1')->get();
            return DataTables::of($data)
                ->addColumn('user', function($data){
                    $a = $data->user->name;
                    return $a;
                })->addColumn('status', function($data){
                    if ($data->active_flag == '1'){
                        $a = '<div class="badge badge-success">active</div>';

                    }else{
                        $a = '<div class="badge badge-danger">non-active</div>';
                    }
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="newGalleries/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a href="'.route('admin.auth.newGalleries.show',$data->id).'" class="btn btn-warning"><i class="fa fa-pencil"></i> Show</a>';
                    $button .= '<a  href="'.route('admin.auth.newGalleriesDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['user','status','action'])
                ->make(true);
        }
        return view('admin.content.newGalleries.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);

        return view('admin.content.newGalleries.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'name' => 'required|unique:new_galleries,name',
            'desc' => 'required',
//            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $create = new NewGalleries();
        $create->user_id = Auth::user()->id;;
        $create->name = $request->name;
        $create->slug = \Str::slug($request->name);
        $create->desc = $request->desc;
        $create->save();

        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file){

                $data = new Galleries();

                $name = rand(999999999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;

                $imgDB = 'uploads/newsGalleries/'.$newName;
                $file->move(public_path('uploads/newsGalleries/'), $newName);

                $data->new_galleries_id = $create->id;
                $data->img = $imgDB;
                $data->format = $extension;
                $data->desc = $imgDB;

                $data->save();
            }
        }
        return redirect()->route('admin.auth.newGalleries.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = NewGalleries::find($id);

        $galleries = Galleries::whereNewGalleriesId($data->id)->get();

        view()->share([
           'data' => $data,
           'galleries' => $galleries
        ]);
        return view('admin.content.newGalleries.show');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NewGalleries::find($id);

        $galleries = Galleries::whereNewGalleriesId($data->id)->get();

        view()->share([
            'data' => $data,
            'galleries' => $galleries
        ]);
        return view('admin.content.newGalleries.form');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'desc' => 'required',
//            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $create = NewGalleries::find($id);
        $create->user_id = Auth::user()->id;;
        $create->name = $request->name;
        $create->slug = \Str::slug($request->name);
        $create->desc = $request->desc;
        $create->save();

        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file){

                $data = new Galleries();

                $name = rand(999999999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;

                $imgDB = 'uploads/newsGalleries/'.$newName;
                $file->move(public_path('uploads/newsGalleries/'), $newName);

                $data->new_galleries_id = $create->id;
                $data->img = $imgDB;
                $data->format = $extension;
                $data->desc = $imgDB;

                $data->save();
            }
        }
        return redirect()->back()->withSuccess('Succcess create data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
