<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agenda;
use App\Models\Category;
use App\Models\News;
use App\Models\Regulation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Agenda::all();
            return DataTables::of($data)
                ->addColumn('user', function($data){
                    $a = $data->user->name;
                    return $a;
                })
                ->addColumn('desc', function($data){
                    $a = \Str::limit($data->desc_banner, 100, $end='......');
                    return $a;
                })->addColumn('active_flag', function($data){
                    if ($data->active_flag == '1'){
                        $a = '<div class="badge badge-success">Active</div>';
                    }else{
                        $a = '<div class="badge badge-danger">non-Active</div>';

                    }
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="agenda/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a href="'.route('admin.auth.agendaSelesaiView',$data->id).'" class="btn btn-warning">Selesai</a>';
                    $button .= '<a  href="'.route('admin.auth.agendaDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['desc','user','active_flag','action'])
                ->make(true);
        }
        return view('admin.content.agenda.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.agenda.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'name' => 'required|unique:agendas,name',
            'desc' => 'required',
            'location' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $data = new Agenda();


        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/agenda/'.$newName;

            $request->image->move(public_path('uploads/agenda/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->location = $request->location;
        $data->date_start = $request->date_start;
        $data->date_end = $request->date_end;
        $data->time = $request->time;
        $data->desc_banner = strip_tags($request->desc);

        $data->save();

        return redirect()->route('admin.auth.agenda.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Agenda::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.agenda.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'desc' => 'required',
            'location' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time' => 'required',
        ];

        $request->validate($validates);

        $data = Agenda::find($id);


        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/agenda/'.$newName;

            $request->image->move(public_path('uploads/agenda/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->desc = $request->desc;
        $data->location = $request->location;
        $data->date_start = $request->date_start;
        $data->date_end = $request->date_end;
        $data->time = $request->time;
        $data->desc_banner = strip_tags($request->desc);
        $data->save();

        return redirect()->route('admin.auth.agenda.index')->withSuccess('Succcess update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Agenda::find($id);


        $data->active_flag = '0';
        $data->save();
        return redirect()->route('admin.auth.agenda.index')->withDanger('Succcess non-active data');

    }

    public function selesai($id)
    {
        $data = Agenda::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.agenda.selesai');

    }
    public function selesaiPost($id,Request $request)
    {
        $validates = [
            'file' => 'required|mimes:pdf,doc,docx',
        ];

        $request->validate($validates);

        $data = Agenda::find($id);

        $data->status = 'sudah berlangsung' ;

        if ($request->file('file')){
            $file = $request->file('file');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/regulation/'.$request->level_id.'/'.$newName;

            $request->file->move(public_path('uploads/regulation/'.$request->level_id.'/'), $newName);
            $data->file = $imgDB;
        }else{
            $data->file = $data->file ;
        }

        $data->save();

        return redirect()->route('admin.auth.agenda.index')->withSuccess('Agenda sudah selesai');

    }
}
