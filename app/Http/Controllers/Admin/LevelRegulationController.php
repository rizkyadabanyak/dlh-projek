<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Regulation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class LevelRegulationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->ajax()) {
            $data = Level::all();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="levels/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.auth.levelsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.level.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.level.form');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Level();

        $data->level = $request->level;

        $data->save();
        return redirect()->route('admin.auth.levels.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Level::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.level.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Level::find($id);

        $data->level = $request->level;

        $data->save();
        return redirect()->route('admin.auth.levels.index')->withSuccess('Succcess update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Level::find($id);

        $data->active_flag = '0';

        $data->save();


        return redirect()->back()->withDanger('Succcess delete data')->withDanger('Succcess delete data');
    }
}
