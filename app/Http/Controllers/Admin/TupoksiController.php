<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Profil;
use App\Models\Tupoksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TupoksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tupoksi::whereActiveFlag('1')->first();
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.tupoksi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.tupoksi.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Tupoksi();

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->content = $request->content;

        $data->save();
        return redirect()->route('admin.auth.tupoksi.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Tupoksi::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.tupoksi.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Tupoksi::find($id);

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->content = $request->content;

        $data->save();
        return redirect()->route('admin.auth.tupoksi.index')->withSuccess('Succcess create data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Tupoksi::find($id);
        $data->active_flag = '0';

        $data->save();

        return redirect()->back()->withDanger('Succcess delete data');
    }
}
