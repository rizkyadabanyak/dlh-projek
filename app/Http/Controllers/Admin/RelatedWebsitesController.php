<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pariwara;
use App\Models\relatedWebsites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class RelatedWebsitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = relatedWebsites::all();
            return DataTables::of($data)
                ->addColumn('user', function($data){
                    $a = $data->user->name;
                    return $a;
                })
                ->addColumn('img', function($data){
                    $a = '<div class="gallery gallery-md"><img class="gallery-item" src="'.asset($data->img).'"></div> ';
                    return $a;
                })->addColumn('active_flag', function($data){
                    if ($data->active_flag == '1'){
                        $a = '<div class="gallery gallery-md"></div><div class="badge badge-success">Active</div>';

                    }else{
                        $a = '<div class="badge badge-danger">non-Active</div>';
                    }
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="relatedWebsites/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.auth.relatedWebsitesDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['user','img','active_flag','action'])
                ->make(true);
        }
        return view('admin.content.relatedWebsites.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.relatedWebsites.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'name' => 'required|unique:related_websites,name',
            'link' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);

        $data = new relatedWebsites();

        if ($request->file('image')) {
            $file = $request->file('image');
            $name = rand(999999999, 1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name . '.' . $extension;
            $imgDB = 'uploads/pariwara/' . $newName;
            $request->image->move(public_path('uploads/pariwara/'), $newName);
            $data->img = $imgDB;
        } else {
            $data->img = $data->img;
        }
        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->url = $request->link;
        $data->save();

        return redirect()->route('admin.auth.relatedWebsites.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = relatedWebsites::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.relatedWebsites.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'link' => 'required',
        ];

        $request->validate($validates);

        $data = relatedWebsites::find($id);

        if ($request->file('image')) {
            $file = $request->file('image');
            $name = rand(999999999, 1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name . '.' . $extension;
            $imgDB = 'uploads/pariwara/' . $newName;
            $request->image->move(public_path('uploads/pariwara/'), $newName);
            $data->img = $imgDB;
        } else {
            $data->img = $data->img;
        }
        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->url = $request->link;
        $data->active_flag = $request->active_flag;

        $data->save();

        return redirect()->route('admin.auth.relatedWebsites.index')->withSuccess('Succcess create data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = relatedWebsites::find($id);

        $data->active_flag = '0';
        $data->save();

        return redirect()->route('admin.auth.relatedWebsites.index')->withDanger('Succcess delete data');;
    }
}
