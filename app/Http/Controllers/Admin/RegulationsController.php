<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Profil;
use App\Models\Regulation;
use App\Models\ViewWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class RegulationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Respon  se
     */
    public function index(Request $request)
    {

        if($request->ajax()) {
            $data = Regulation::whereActiveFlag('1')->get();
            return DataTables::of($data)
                ->addColumn('level_id', function($data){
                    $a = $data->level->level;
                    return $a;
                })->addColumn('file', function($data){
                    $button = '<a  href="'.url($data->file).'" class="btn btn-info" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-download"></i> Download</a>';
                    return $button;
                })->addColumn('action', function($data){

                    $button = '<a href="regulations/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    if ($data->status == 'Aktif'){
                        $button .= '<a  href="'.route('admin.auth.regulationsDestroyStatus',$data->id).'" class="btn btn-warning" onclick="return confirm('.'`Are you sure?`'.')">Cabut</a>';
                    }else{
                        $button .= '<a  href="'.route('admin.auth.regulationsDestroyStatus',$data->id).'" class="btn btn-success" onclick="return confirm('.'`Are you sure?`'.')">Aktif</a>';

                    }
                    $button .= '<a  href="'.route('admin.auth.regulationsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['level_id','file','action'])
                ->make(true);
        }
        return view('admin.content.regulation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $levels = Level::all();
        view()->share([
            'data' => $data,
            'levels' => $levels
        ]);
        return view('admin.content.regulation.form');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'name' => 'required',
            'file' => 'required|mimes:pdf,doc,docx',
        ];

        $request->validate($validates);

        $data = new Regulation();

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->nomer = $request->nomer;
        $data->year = $request->year;

        $data->level_id = $request->level_id;

        if ($request->file('file')){
            $file = $request->file('file');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/regulation/'.$request->level_id.'/'.$newName;

            $request->file->move(public_path('uploads/regulation/'.$request->level_id.'/'), $newName);
            $data->file = $imgDB;
        }else{
            $data->file = $data->file ;
        }

        $data->save();

        return redirect()->route('admin.auth.regulations.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Regulation::find($id);
        $levels = Level::all();

        view()->share([
            'data' => $data,
            'levels' => $levels
        ]);
        return view('admin.content.regulation.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Regulation::find($id);

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->nomer = $request->nomer;
        $data->year = $request->year;

        $data->level_id = $request->level_id;

        if ($request->file('file')){
            $file = $request->file('file');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/regulation/'.$request->level_id.'/'.$newName;

            $request->file->move(public_path('uploads/regulation/'.$request->level_id.'/'), $newName);
            $data->file = $imgDB;
        }else{
            $data->file = $data->file ;
        }

        $data->save();

        return redirect()->route('admin.auth.regulations.index')->withSuccess('Succcess edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {

        $data = Regulation::find($id);

        if ($data->status == 'Aktif'){
            $data->status = 'Sudah di Cabut';
        }else{
            $data->status = 'Aktif';
        }

        $data->save();
        return redirect()->back()->withWarning('Succcess edit data');
    }
    public function destroy($id)
    {
        $data = Regulation::find($id);
        $data->active_flag = '0';

        $data->save();
        return redirect()->back()->withDanger('Succcess delete data');
    }
}
