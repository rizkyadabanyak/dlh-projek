<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\InformationPublic;
use App\Models\PermohonanPPID;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class InformationPPIDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = InformationPublic::orderBy('created_at','DESC')->get();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="informationPPID/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.auth.informationPPIDDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.informationPPID.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
           'data' => $data
        ]);
        return view('admin.content.informationPPID.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
            'title' => 'required',
            'ringkasan' => 'required',
            'pejabat' => 'required',
            'pj' => 'required',
            'waktudantempat' => 'required',
            'bentuk' => 'required',
            'jangka_waktu' => 'required',
        ];

        $request->validate($validates);
        $data = new InformationPublic();

        $data->title = $request->title;
        $data->ringkasan = $request->ringkasan;
        $data->pejabat = $request->pejabat;
        $data->pj = $request->pj;
        $data->waktudantempat = $request->waktudantempat;
        $data->bentuk = $request->bentuk;
        $data->jangka_waktu = $request->jangka_waktu;

        $data->save();

        return redirect()->route('admin.auth.informationPPID.index')->withSuccess('sukses tambah data');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = InformationPublic::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.informationPPID.form');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'title' => 'required',
            'ringkasan' => 'required',
            'pejabat' => 'required',
            'pj' => 'required',
            'waktudantempat' => 'required',
            'bentuk' => 'required',
            'jangka_waktu' => 'required',
        ];

        $request->validate($validates);
        $data = InformationPublic::find($id);

        $data->title = $request->title;
        $data->ringkasan = $request->ringkasan;
        $data->pejabat = $request->pejabat;
        $data->pj = $request->pj;
        $data->waktudantempat = $request->waktudantempat;
        $data->bentuk = $request->bentuk;
        $data->jangka_waktu = $request->jangka_waktu;

        $data->save();

        return redirect()->route('admin.auth.informationPPID.index')->withSuccess('sukses tambah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
