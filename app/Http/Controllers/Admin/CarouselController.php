<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agenda;
use App\Models\Carousel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Carousel::all();
            return DataTables::of($data)
                ->addColumn('user', function($data){
                    $a = $data->user->name;
                    return $a;
                })->addColumn('active_flag', function($data){
                    if ($data->active_flag == '1'){
                        $a = '<div class="badge badge-success">Active</div>';

                    }else{
                        $a = '<div class="badge badge-danger">non-Active</div>';

                    }
                    return $a;
                })->addColumn('action', function($data){
                    $button = '<a href="carousel/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    if ($data->active_flag == '1'){
                        $button .= '<a  href="'.route('admin.auth.carouselDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';

                    }else{
                        $button .= '<a  href="'.route('admin.auth.carouselActive',$data->id).'" class="btn btn-success" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-check"></i> Aktifkan</a>';

                    }
                    return $button;
                })
                ->rawColumns(['user','active_flag','action'])
                ->make(true);
        }
        return view('admin.content.carousel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.carousel.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Carousel();
        $validates = [
            'name' => 'required|unique:agendas,name',
            'desc' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,svg'
        ];

        $request->validate($validates);


        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/agenda/'.$newName;

            $request->image->move(public_path('uploads/agenda/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->desc = $request->desc;
        $data->save();

        return redirect()->route('admin.auth.carousel.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Carousel::find($id);

        view()->share([
            'data' => $data
        ]);

        return view('admin.content.carousel.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Carousel::find($id);
        $validates = [
            'name' => 'required|unique:agendas,name',
            'desc' => 'required',
        ];

        $request->validate($validates);


        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/agenda/'.$newName;

            $request->image->move(public_path('uploads/agenda/'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $data->img ;
        }

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->desc = $request->desc;
        $data->save();

        return redirect()->route('admin.auth.carousel.index')->withSuccess('Succcess edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Carousel::find($id);
        $data->active_flag = '0';

        $data->save();


        return redirect()->route('admin.auth.carousel.index')->withDanger('Succcess Delete data');

    }

    public function carouselActive($id){
        $data = Carousel::find($id);
        $data->active_flag = '1';

        $data->save();


        return redirect()->route('admin.auth.carousel.index')->withSuccess('Succcess mengaktifkan data');
    }
}
