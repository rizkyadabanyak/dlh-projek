<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Profil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Profil::whereActiveFlag('1')->first();
        view()->share([
           'data' => $data
        ]);
        return view('admin.content.profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.profile.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Profil();

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->content = $request->content;

        $data->save();
        return redirect()->route('admin.auth.profile.index')->withSuccess('Succcess create data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Profil::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.profile.form');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Profil::find($id);

        $data->user_id = Auth::user()->id;
        $data->name = $request->name;
        $data->content = $request->content;

        $data->save();
        return redirect()->route('admin.auth.profile.index')->withWarning('Succcess update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Profil::find($id);
        $data->active_flag = '0';

        $data->save();

        return redirect()->back()->withDanger('Succcess delete data')->withDanger('Succcess delete data');
    }
}
