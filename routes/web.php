<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[\App\Http\Controllers\HomeController::class,'index'])->name('home');


Auth::routes();
Route::group(['prefix'=>'berita'],function () {
    Route::get('{slug}',[\App\Http\Controllers\HomeController::class,'detail'])->name('detail');
});
Route::group(['prefix'=>'agenda'],function () {
    Route::get('{slug}',[\App\Http\Controllers\HomeController::class,'detailAgenda'])->name('detailAgenda');
});

Route::group(['prefix'=>'detailBeritaFoto'],function () {
    Route::get('{slug}',[\App\Http\Controllers\HomeController::class,'detailBeritaFoto'])->name('detailBeritaFoto');
});

Route::get('ppid',[\App\Http\Controllers\HomeController::class,'ppid'])->name('ppid');
Route::post('ppid/search',[\App\Http\Controllers\HomeController::class,'searchActionPPID'])->name('searchActionPPID');
Route::get('ppid/permohonan',[\App\Http\Controllers\HomeController::class,'ppidPermohonan'])->name('ppidPermohonan');
Route::post('ppid/permohonan',[\App\Http\Controllers\HomeController::class,'ppidPermohonanAction'])->name('ppidPermohonanAction');
Route::get('categori/berita/{slug}',[\App\Http\Controllers\HomeController::class,'categorySelect'])->name('categorySelect');


Route::get('more-new',[\App\Http\Controllers\HomeController::class,'moreNew'])->name('moreNew');
Route::post('search',[\App\Http\Controllers\HomeController::class,'searchAction'])->name('searchAction');
Route::get('search/{name}',[\App\Http\Controllers\HomeController::class,'search'])->name('search');
Route::get('profile',[\App\Http\Controllers\HomeController::class,'profile'])->name('profile');
Route::get('tupoksi',[\App\Http\Controllers\HomeController::class,'tupoksi'])->name('tupoksi');
Route::get('struktur',[\App\Http\Controllers\HomeController::class,'structure'])->name('structure');
Route::get('peraturan',[\App\Http\Controllers\HomeController::class,'regulations'])->name('regulations');
Route::get('peraturan/{id}',[\App\Http\Controllers\HomeController::class,'regulationsFilter'])->name('regulationsFilter');


Route::get('downloads',[\App\Http\Controllers\HomeController::class,'downloads'])->name('downloads');
Route::get('landing-pengaduan',[\App\Http\Controllers\HomeController::class,'landingPengaduan'])->name('landingPengaduan');
Route::get('pengaduan',[\App\Http\Controllers\HomeController::class,'pengaduan'])->name('pengaduan');
Route::post('pengaduan',[\App\Http\Controllers\HomeController::class,'pengaduanAction'])->name('pengaduanAction');
Route::post('cek/pengaduan',[\App\Http\Controllers\HomeController::class,'pengaduanActionCek'])->name('pengaduanActionCek');
//Route::get('laporan',[\App\Http\Controllers\HomeController::class,'report'])->name('report');
Route::get('agenda/select/{date}',[\App\Http\Controllers\HomeController::class,'agendaSelectDate'])->name('agendaSelectDate');

Route::post('news/comment/{id}/{cek}',[\App\Http\Controllers\HomeController::class,'commentNew'])->name('commentNew');

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth']],function (){

    Route::group(['as'=>'auth.'],function (){
        Route::get('dashboard',[\App\Http\Controllers\Admin\DashboardController::class,'index'])->name('dashboard');

        Route::get('log/clients',[\App\Http\Controllers\Admin\DashboardController::class,'logClients'])->name('logClients');

        Route::resource('users', \App\Http\Controllers\Admin\UserController::class);
        Route::get('users/destroy/{id}',[\App\Http\Controllers\Admin\UserController::class,'destroy'])->name('usersDestroy');

        Route::resource('pengaduan', \App\Http\Controllers\Admin\PengaduanController::class);
        Route::get('pengaduan/destroy/{id}',[\App\Http\Controllers\Admin\PengaduanController::class,'destroy'])->name('pengaduanDestroy');

        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
        Route::get('categories/destroy/{id}',[\App\Http\Controllers\Admin\CategoryController::class,'destroy'])->name('categoriesDestroy');

        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
        Route::get('categories/destroy/{id}',[\App\Http\Controllers\Admin\CategoryController::class,'destroy'])->name('categoriesDestroy');

        Route::resource('news', \App\Http\Controllers\Admin\NewsController::class);
        Route::get('news/destroy/{id}',[\App\Http\Controllers\Admin\NewsController::class,'destroy'])->name('newsDestroy');

        Route::resource('newGalleries', \App\Http\Controllers\Admin\NewGalleriesController::class);
        Route::get('newGalleries/destroy/{id}',[\App\Http\Controllers\Admin\NewGalleriesController::class,'destroy'])->name('newGalleriesDestroy');
        Route::post('newGalleries/selectDelete', [\App\Http\Controllers\Admin\NewGalleriesController::class,'selectDelete'])->name('newGalleries.selectDelete');

        Route::resource('profile', \App\Http\Controllers\Admin\ProfileController::class);
        Route::get('profile/destroy/{id}',[\App\Http\Controllers\Admin\ProfileController::class,'destroy'])->name('profileDestroy');

        Route::resource('tupoksi', \App\Http\Controllers\Admin\TupoksiController::class);
        Route::get('tupoksi/destroy/{id}',[\App\Http\Controllers\Admin\TupoksiController::class,'destroy'])->name('tupoksiDestroy');

        Route::resource('structure', \App\Http\Controllers\Admin\StructureController::class);
        Route::get('structure/destroy/{id}',[\App\Http\Controllers\Admin\StructureController::class,'destroy'])->name('structureDestroy');

        Route::resource('regulations', \App\Http\Controllers\Admin\RegulationsController::class);
        Route::get('regulations/destroy/{id}',[\App\Http\Controllers\Admin\RegulationsController::class,'destroy'])->name('regulationsDestroy');
        Route::get('regulations/destroyStatus/{id}',[\App\Http\Controllers\Admin\RegulationsController::class,'status'])->name('regulationsDestroyStatus');

        Route::resource('levels', \App\Http\Controllers\Admin\LevelRegulationController::class);
        Route::get('levels/destroy/{id}',[\App\Http\Controllers\Admin\LevelRegulationController::class,'destroy'])->name('levelsDestroy');

        Route::resource('agenda', \App\Http\Controllers\Admin\AgendaController::class);
        Route::get('agenda/destroy/{id}',[\App\Http\Controllers\Admin\AgendaController::class,'destroy'])->name('agendaDestroy');
        Route::get('agenda/selesaiView/{id}',[\App\Http\Controllers\Admin\AgendaController::class,'selesai'])->name('agendaSelesaiView');
        Route::post('agenda/selesaiPost/{id}',[\App\Http\Controllers\Admin\AgendaController::class,'selesaiPost'])->name('agendaSelesaiPost');

        Route::resource('carousel', \App\Http\Controllers\Admin\CarouselController::class);
        Route::get('carousel/destroy/{id}',[\App\Http\Controllers\Admin\CarouselController::class,'destroy'])->name('carouselDestroy');
        Route::get('carousel/active/{id}',[\App\Http\Controllers\Admin\CarouselController::class,'carouselActive'])->name('carouselActive');

        Route::resource('pariwara', \App\Http\Controllers\Admin\PariwaraController::class);
        Route::get('pariwara/destroy/{id}',[\App\Http\Controllers\Admin\PariwaraController::class,'destroy'])->name('pariwaraDestroy');

        Route::resource('relatedWebsites', \App\Http\Controllers\Admin\RelatedWebsitesController::class);
        Route::get('relatedWebsites/destroy/{id}',[\App\Http\Controllers\Admin\RelatedWebsitesController::class,'destroy'])->name('relatedWebsitesDestroy');

        Route::resource('report', \App\Http\Controllers\Admin\ReportController::class);
        Route::get('report/destroy/{id}',[\App\Http\Controllers\Admin\ReportController::class,'destroy'])->name('reportDestroy');

        Route::resource('complaint', \App\Http\Controllers\Admin\ComplaintController::class);
        Route::get('complaint/destroy/{id}',[\App\Http\Controllers\Admin\ComplaintController::class,'destroy'])->name('complaintDestroy');
        Route::get('complaint/tindaklanjut/{id}',[\App\Http\Controllers\Admin\ComplaintController::class,'tindaklanjut'])->name('complaintTindaklanjut');
        Route::get('complaint/selesai/{id}',[\App\Http\Controllers\Admin\ComplaintController::class,'selesai'])->name('complaintSelesai');

        Route::resource('ppid', \App\Http\Controllers\Admin\PpidController::class);
        Route::get('ppid/destroy/{id}',[\App\Http\Controllers\Admin\PpidController::class,'destroy'])->name('ppidDestroy');

        Route::resource('permohonan', \App\Http\Controllers\Admin\PermohonanController::class);
        Route::get('permohonan/destroy/{id}',[\App\Http\Controllers\Admin\PermohonanController::class,'destroy'])->name('permohonanDestroy');

        Route::resource('informationPPID', \App\Http\Controllers\Admin\InformationPPIDController::class);
        Route::get('informationPPID/destroy/{id}',[\App\Http\Controllers\Admin\InformationPPIDController::class,'destroy'])->name('informationPPIDDestroy');

        Route::resource('videoProfile', \App\Http\Controllers\Admin\VideoProfileController::class);
        Route::get('videoProfile/destroy/{id}',[\App\Http\Controllers\Admin\VideoProfileController::class,'destroy'])->name('videoProfileDestroy');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
