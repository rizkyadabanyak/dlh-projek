@extends('landing.app')

@section('content')
    @include('landing.components.style-search')
    <style>

        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }


        .green{
            background-color:#6fb936;
        }
        .thumb{
            margin-bottom: 30px;
        }

        .page-top{
            margin-top:85px;
        }


        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }


        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }
        .modal-header {

            border-bottom: none;
        }
        .modal-title {
            color:#000;
        }
        .modal-footer{
            display:none;
        }

    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5">
        <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">PPID</a><br>

        <div class="row d-flex justify-content-center">


            <div class="col-md-12">
                <h3 class="heading mt-5 text-center">Hy, Apakah Anda Ingin Mencari Berita Mengenai PPID ? </h3>
                <form action="{{route('searchActionPPID')}}" method="post">
                    @csrf
                    <div class="d-flex justify-content-center px-5">
                        <div class="search"> <input type="text" class="typeahead search-input" placeholder="Search..." name="search"> <button class="search-icon"> <i class="fa fa-search"></i> </button> </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12">
                <h3 class="heading mt-5 text-center">PERMOHONAN INFORMASI PUBLIK

                </h3>

                <div class="d-flex justify-content-center px-5">
                    <a href="{{route('ppidPermohonan')}}" class="btn btn-success">
                        Ajukan
                    </a>
                </div>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Berita PPID</a>
                <br><br>

                <div class="row">
                    @forelse($news as $new)
                        <div class="col-12 mt-3">
                            <div class="card" style="border: 0px">
                                <div class="card-horizontal">
                                    <div class="img-square-wrapper">
                                        <img class="img-card" src="{{asset($new->img)}}" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <a href="{{route('detail',$new->slug)}}" style="color: black">
                                            <p class="card-title" style="font-family: 'Segoe UI';font-size: 20px">{{\Str::limit($new->name, 70, $end='...') }}</p>
                                        </a>
                                        <p class="card-text" style="font-family: 'Segoe UI';color: #5ca863">{{$new->category->name}}</p>
                                        <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);">{!! \Str::limit($new->desc_banner, 200, $end='......') !!}</p>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('detail',$new->slug)}}" style="text-decoration: none;color: #5ca863;font-size: 15px">Read More</a>

                        </div>
                    @empty
                        <h5 class="mt-5">belum ada berita PPID</h5>
                    @endforelse

                </div>
            </div>
            <div class="col-md-6">
                <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">DAFTAR INFORMASI PUBLIK</a>
                <br><br>
                <div class="table-responsive">

                    <table id="example" class="table table-striped table-bordered " style="width:100%">
                        <thead>
                        <tr>
                            <th>Nama / Judul Informasi</th>
                            <th>Ringkasan Isi Informasi</th>
                            <th>Pejabat/Unit/Satuan
                                Kerja yang Menguasai Informasi
                            </th>
                            <th>Penanggung Jawab Pembuatan/Penerbitan Informasi</th>
                            <th>Waktu dan Tempat Pembuatan Informasi</th>
                            <th>Bentuk Informasi yang Tersedia</th>
                            <th>Jangka Waktu Penyimpanan / Masa Retensi Arsip</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($informations as $information)
                            <tr>
                                <td>{{$information->title}}</td>
                                <td>{{$information->ringkasan}}</td>
                                <td>{{$information->pejabat}}</td>
                                <td>{{$information->pj}}</td>
                                <td>{{$information->waktudantempat}}</td>
                                <td>{{$information->bentuk}}</td>
                                <td>{{$information->jangka_waktu}}</td>
                            </tr>
                        @endforeach


                    </table>
                </div>
            </div>
        </div>
        <br>
        <div style="float: right">
            {{ $news->links('vendor.pagination.bootstrap-4') }}
        </div>
        <br>

    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{asset('datatabel/datatabel.js')}}"></script>


    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection
