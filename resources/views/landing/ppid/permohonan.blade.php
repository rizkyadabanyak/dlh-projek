@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px;">
                <div class="row">
                    <div class="card shadow-card" style="width: 100%">
                        <div class="row">
                            <div class="col-sm-5 col-md-12 col-12 pb-4">
                                <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Form Permohonan Informasi</a><br><br>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @include('admin.components.partials.message')
                                <form id="regForm" action="{{route('ppidPermohonanAction')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="tab">
                                        <div class="form-group"> <label for="name">Nama</label> <input type="text" name="name" id="fullname" class="form-control"> </div>
                                        <div class="form-group"> <label for="email">Email</label> <input type="text" name="email" id="email" class="form-control"> </div>
                                        <div class="form-group"> <label for="name">Telepon</label> <input type="text" name="phone" id="fullname" class="form-control"> </div>
                                        <div class="form-group"> <label for="name">Tanggal Lahir</label> <input type="date" name="date_of_birth"  class="form-control"> </div>
                                        <div class="form-group">
                                            <label for="message">Rincian Informasi</label>
                                            <textarea name="detail" id="" msg cols="30" rows="5" class="form-control" style="background-color: #ffffff;"></textarea>
                                        </div>
                                        <div class="form-group"> <label for="name">Tujuan Pembuatan informai</label> <input type="text" name="target" id="fullname" class="form-control"> </div>
                                        <div class="form-group">
                                            <label for="message">Cara Memperoleh Informasi</label>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="memperoleh[]" value="Melihat/membaca/mendengarkan/mencatat" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Melihat/ membaca/ mendengarkan/ mencatat
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="memperoleh[]" value="Mendapatkan salinan informasi (hardcopy/softcopy)***" id="flexCheckChecked">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Mendapatkan salinan informasi (hardcopy/softcopy)***
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="message">Mendapatkan Salinan Informasi***</label>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="mendapatkan[]" value="Mengambil Langsung" id="a">
                                                <label class="form-check-label" for="a">
                                                    Mengambil Langsung
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="mendapatkan[]" value="Kurir" id="b">
                                                <label class="form-check-label" for="b">
                                                    Kurir
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="mendapatkan[]" value="Pos" id="c">
                                                <label class="form-check-label" for="c">
                                                    Pos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="mendapatkan[]" value="Email" id="d">
                                                <label class="form-check-label" for="d">
                                                    Email
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="mendapatkan[]" value="Faksimili" id="e">
                                                <label class="form-check-label" for="e">
                                                    Faksimili
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="overflow:auto;">
                                        <div style="float:right;">
                                            <button class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                                <!-- Circles which indicates the steps of the form: -->
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }
    </script>

@endsection
