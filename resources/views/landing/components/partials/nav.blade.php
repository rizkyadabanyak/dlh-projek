<nav class="navbar navbar-expand-md fixed-top navbar-dark padding-a" style="padding-right: 100px;padding-left: 100px;padding-top: 10px;padding-bottom: 10px">
    <a class="navbar-brand" href="#">
        <img src="{{asset('uploads/landing/dlh-logo-putih.png')}}" width="230" height="70" alt="">
    </a>
    <button style="background: #7ec37e" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="{{route('ppid')}}">PPID</a>
            </li>
            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Profil
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('profile')}}" style="font-weight: lighter">PROFILE</a>
                    <a class="dropdown-item" href="{{route('tupoksi')}}">TUPOKSI</a>
                    <a class="dropdown-item" href="{{route('structure')}}">STRUKTUR</a>
{{--                    <a class="dropdown-item" href="{{route('regulations')}}">PERATURAN/UU</a>--}}
                </div>
            </li>
            <li class="nav-item active" style="margin-right: 10px">
                <a class="nav-link" href="{{route('landingPengaduan')}}">E-Pengaduan <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Publikasi data
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    {{--                    <a class="dropdown-item" href="{{route('downloads')}}" style="font-weight: lighter">DOWNLOAD</a>--}}
                    <a class="dropdown-item" href="{{route('regulations')}}">Peraturan</a>
                    <a class="dropdown-item" href="{{route('downloads')}}">Laporan</a>
                </div>
            </li>
            <li class="nav-item dropdown active" style="margin-right: 10px">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    LAINNYA
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
{{--                    <a class="dropdown-item" href="{{route('downloads')}}" style="font-weight: lighter">DOWNLOAD</a>--}}
{{--                    <a class="dropdown-item" href="{{route('pengaduan')}}">PENGADUAN</a>--}}
{{--                    <a class="dropdown-item" href="#">E-IKPLHD</a>--}}
                    <a class="dropdown-item" href="#">E-SIMONIKA</a>
                    <a class="dropdown-item" href="#">UPT LAB</a>
                    <a class="dropdown-item" href="#">EKINERJA</a>
                </div>
            </li>
            <li class="nav-item">
                <label class="theme-switch" for="checkbox">
                    <input type="checkbox" id="checkbox"/>
                    <div class="slider round"></div>
                </label>
            </li>

        </ul>
    </div>

</nav>

