<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
                <a href="index.html">Dlh</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Dlh</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                    <li class="active"><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                </ul>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.categories.index')}}">Categories</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.news.index')}}">News</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.newGalleries.index')}}">New Galleries</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.agenda.index')}}">Agenda</a></li>

                </ul>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>About Us</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.profile.index')}}">Profile</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.tupoksi.index')}}">Tupoksi</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.structure.index')}}">Structure</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.regulations.index')}}">Regulations/UU</a></li>

                </ul>
            </li>
            <li><a class="nav-link" href="credits.html"><i class="fas fa-pencil-ruler"></i> <span>Credits</span></a></li>
        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
            </a>
        </div>
    </aside>
</div>
