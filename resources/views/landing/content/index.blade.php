@extends('landing.app')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
{{--    <script src="custom.js"></script>--}}

    {{--    @include('landing.components.style-card')--}}
    @include('landing.components.style-search')
    <style>

        #demo {
            height:100%;
            position:relative;
            overflow:hidden;
        }


        .green{
            background-color:#6fb936;
        }
        .thumb{
            margin-bottom: 30px;
        }

        .page-top{
            margin-top:85px;
        }


        img.zoom {
            width: 100%;
            height: 200px;
            border-radius:5px;
            object-fit:cover;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }


        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }
        .modal-header {

            border-bottom: none;
        }
        .modal-title {
            color:#000;
        }
        .modal-footer{
            display:none;
        }

    </style>

    <div id="carousel" class="carousel carousel-dark slide" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <a href="#">
                    <!--
                    If you need more browser support use https://scottjehl.github.io/picturefill/
                    If a picture looks blurry on a retina device you can add a high resolution like this
                    <source srcset="img/blog-post-1000x600-2.jpg, blog-post-1000x600-2@2x.jpg 2x" media="(min-width: 768px)">

                    What image sizes should you use? This can help - https://codepen.io/JacobLett/pen/NjramL
                     -->
                    <picture>
                        <div style="background-color: black">
                            <img style="opacity: 50%;height: 100vh" srcset="{{asset($carouselsFirst->img)}}" alt="responsive image" class="d-block img-fluid">
                        </div>
                    </picture>

                    <div class="carousel-caption">
                        <div>
                            <h2>{{$carouselsFirst->name}}</h2>
                            <p style="color: #c2c2c2">{{$carouselsFirst->desc}}</p>
                            {{--                            <span class="btn btn-sm btn-outline-secondary">Read More</span>--}}
                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
            @foreach($carousels as $carousel)

                @if($carousel->id != $carouselsFirst->id)
                    <div class="carousel-item">
                        <a href="#">
                            <picture>
                                <div style="background-color: black">
                                    {{--                                <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">--}}

                                    {{--                                <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">--}}
                                    {{--                                <source srcset="https://dummyimage.com/1400x500/007aeb/4196e5" media="(min-width: 769px)">--}}
                                    {{--                                <source srcset="https://dummyimage.com/800x500/007aeb/4196e5" media="(min-width: 577px)">--}}
                                    <img style="opacity: 50%;height: 100vh" srcset="{{$carousel->img}}" alt="responsive image" class="d-block img-fluid">
                                </div>
                            </picture>

                            <div class="carousel-caption justify-content-center align-items-center">
                                <div class="carousel-caption">
                                    <div>
                                        <h2>{{$carousel->name}}</h2>
                                        <p style="color: #c2c2c2">{{$carousel->desc}}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @else

                @endif

            @endforeach
            <!-- /.carousel-item -->
{{--            <div class="carousel-item">--}}
{{--                <a href="https://bootstrapcreative.com/">--}}
{{--                    <picture>--}}
{{--                        <div style="background-color: black">--}}
{{--                            <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">--}}
{{--                            <source srcset="https://dummyimage.com/1400x500/007aeb/4196e5" media="(min-width: 769px)">--}}
{{--                            <source srcset="https://dummyimage.com/800x500/007aeb/4196e5" media="(min-width: 577px)">--}}
{{--                            <img style="opacity: 50%;height: 100vh" srcset="https://dummyimage.com/1400x500/007aeb/4196e5" alt="responsive image" class="d-block img-fluid">--}}
{{--                        </div>--}}
{{--                    </picture>--}}

{{--                    <div class="carousel-caption justify-content-center align-items-center">--}}
{{--                        <div>--}}
{{--                            <h2>Performance Optimization</h2>--}}
{{--                            <p>We monitor and optimize your site's long-term performance</p>--}}
{{--                            <span class="btn btn-sm btn-secondary">Learn How</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <!-- /.carousel-item -->--}}
        </div>
        <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
    <div class="container py-5" style="max-width: 1200px">

        <section class="wrapper">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <h3 class="heading mt-5 text-center" >Hy, Apakah Anda Ingin Mencari Berita ? </h3>
                    <form action="{{route('searchAction')}}" method="post" class="mb-5">
                        @csrf
                        <div class="d-flex justify-content-center px-5">
                            <div class="search"> <input type="text" class="typeahead search-input" placeholder="Search..." name="search"> <button class="search-icon"> <i class="fa fa-search"></i> </button> </div>
                        </div>
                    </form>
                    <div class="row mt-2 g-1 px-4 mb-5">
                        <div class="col-md-4 mt-3">
                            <a href="{{route('downloads')}}">

                                <div class="card-inner p-3 d-flex flex-column align-items-center" style="background-color: var(--color-bg);"> <img src="{{asset('assets/file-download-solid.svg')}}" width="50" style="padding-bottom: 5px" height="65">
                                    <div class="text-center mg-text"> <span class="mg-text" style="color: var(--color-font);">Download Dokumen</span> </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 mt-3">
                            <a href="{{route('landingPengaduan')}}">

                                <div class="card-inner p-3 d-flex flex-column align-items-center" style="background-color: var(--color-bg);"> <img src="{{asset('assets/comments-solid.svg')}}" width="50" height="65">
                                    <div class="text-center mg-text"> <span class="mg-text" style="color: var(--color-font);">Pengaduan</span> </div>
                                </div>
                            </a>

                        </div>
                        <div class="col-md-4 mt-3">
                            <div class="card-inner p-3 d-flex flex-column align-items-center" style="background-color: var(--color-bg);"> <img src="{{asset('assets/camera-retro-solid.svg')}}" width="50" height="65">
                                <div class="text-center mg-text"> <span class="mg-text" style="color: var(--color-font);">Foto kegiatan</span> </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>


            <div class="row">
                <div class="col-md-8">
                    <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Berita Terbaru</a>
                    <p href="" class="mt-2">Menyajikan berita terkini seputar DLH provinsi jawa timur</p>
                    <div class="row">
                        @forelse($news as $new)
                            <div class="col-12 mt-3">
                                <div class="card" style="border: 0px">
                                    <div class="card-horizontal">
                                        <div class="img-square-wrapper">
                                            <img class="img-card" src="{{asset($new->img)}}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <a href="{{route('detail',$new->slug)}}" style="color: black">
                                                <p class="card-title" style="font-family: 'Segoe UI';font-size: 20px">{{\Str::limit($new->name, 70, $end='...') }}</p>
                                            </a>
                                            <p class="card-text" style="font-family: 'Segoe UI';color: #5ca863">{{$new->category->name}}</p>
                                            <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);">{!! \Str::limit($new->desc_banner, 100, $end='......') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{route('detail',$new->slug)}}" style="text-decoration: none;color: #5ca863;font-size: 15px">Read More</a>
                                <hr size="10px" style="color: rgba(0,0,0,0.45);border: 1px solid" />
                            </div>
                        @empty
                            <h5 class="mt-5">belum ada berita</h5>
                        @endforelse
                    </div>


                    <div class="d-flex justify-content-between">
                        <div>
                        </div>
                        <div>
                            <a class="btn btn-success btn-sm mt-3 text-right" href="{{route('moreNew')}}"> Selengkapnya >></a>

                        </div>
                    </div>
                    <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Pariwara</a>
                    <p href="" class="mt-2">Menyajikan informasi mengenai pariwara Pemerintahan provinsi jawa timur</p>
                    <br><br>
                    <div class="row">
                        @forelse($pariwaras as $pariwara)
                            <div class="col-lg-3 col-md-3 col-xs-6 thumb">
                                <a href="{{$pariwara->link}}" class="fancybox" rel="ligthbox">
                                    <img  src="{{asset($pariwara->img)}}" class="zoom img-fluid "  alt="">

                                </a>
                            </div>
                        @empty

                            <h1>PARIWARAS BELUM ADA</h1>
                        @endforelse


                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-12 ">
                            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Kategori Berita</a>
                            <p href="" class="mt-2">Menyajikan Topik Kategori dari Berita</p>
                            <br>
                            <ul class="list-group">
                                @forelse($categories as $category)
                                    <a href="{{route('categorySelect',$category->slug)}}">
                                        <li class="list-group-item" style="background-color: var(--color-bg);">{{$category->name}}</li>
                                    </a>

                                @empty
                                    <li class="list-group-item">Belum ada category</li>

                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3">
                            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Profile</a>
                            <p href="" class="mt-2">Vidio Profile DLH Jatimprov</p>
                            <video width="380px" controls>
                                <source src="{{asset($video->src)}}">
                            </video>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 mt-3">
                            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Agenda</a>
                            <p href="" class="mt-2">Menyajikan agenda terkini di DLH provinsi jawa timur</p>
                            <div id="myCalendar" class="vanilla-calendar"></div>

                            {{--                            <div class="row">--}}
{{--                                @forelse($agendas as $agenda)--}}
{{--                                    <div class="col-12">--}}
{{--                                        <div class="card" style="border: 0px;padding-top: 2px;padding-bottom: 2px">--}}
{{--                                            <div class="card-horizontal">--}}
{{--                                                <div class="img-square-wrapper">--}}
{{--                                                    <img class="img-card" src="{{asset($agenda->img)}}" alt="Card image cap" style="width: 100px;height: 100px;margin-top: 20px">--}}
{{--                                                </div>--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <a href="{{route('detailAgenda',$agenda->slug)}}" style="color: black">--}}
{{--                                                        <p class="card-title" style="font-family: 'Segoe UI';font-size: 10px;font-weight: bold">{{\Str::limit($agenda->name, 70, $end='...') }}</p>--}}
{{--                                                    </a>--}}
{{--                                                    <p class="card-text" style="font-family: 'Segoe UI';color: #5ca863;font-size: 10px">{{\Carbon\Carbon::parse($agenda->date_start)->toFormattedDateString()}} - {{\Carbon\Carbon::parse($agenda->date_end)->toFormattedDateString()}} </p>--}}
{{--                                                    <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);font-size: 10px">{!! \Str::limit($agenda->desc_banner, 40, $end='...') !!}</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                @empty--}}
{{--                                    <h5 class="mt-5">belum ada berita</h5>--}}
{{--                                @endforelse--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <br><br>




            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">Gallery</a>
            <p href="" class="mt-2">Menyajikan informasi dari Pemerintahan provinsi jawa timur yang disajikan dalam bentuk foto dan Vidio</p>
            <div class="row">
                @forelse($newGalleries as $newGallery)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    @forelse($newGallery->images->take(1) as $a)
                        <a href="{{route('detailBeritaFoto',$newGallery->slug)}}" class="fancybox" rel="ligthbox">
                            <img  src="{{asset($a->img)}}" class="zoom img-fluid "  alt="{{$newGallery->name}}">
                        </a>
                    @empty
                        <h5>belum ada foto</h5>
                    @endforelse
                </div>
                @empty
                    <h4>belum ada data</h4>
                @endforelse



            </div>

            <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">WEBSITE TERKAIT</a>
            <p href="" class="mt-2">Menyajikan Website Terkait dari DLH Jatimprov</p>
            <center>
            <div class="row">
                @foreach($relatedWebsites as $relatedWebsite)
                <div class="col-md-2 p-1" style=" /* Center vertically and horizontally */
  display: flex;
  justify-content: center;
  align-items: center;">
                    <a href="{{$relatedWebsite->url}}">
                        <img src="{{asset($relatedWebsite->img)}}" width="100" class="mx-auto d-block" alt="...">
                    </a>
                </div>
                    @endforeach
            </div>
            </center>
        </section>
    </div>
    <script>
        function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat)
            return [pad(d.getFullYear()), pad(d.getMonth()+1), d.getDate()].join('-')
        }
        let elem = document.querySelector('#myCalendar');

        let myCalendar = new VanillaCalendar({
            selector: "#myCalendar",
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            shortWeekday: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            onSelect: (data, elem) => {
                date = convertDate(data.date)
                console.log(convertDate(data.date))
                window.location = 'agenda/select/'+date;

            }
        })
        myCalendar.init();

        // elem.addEventListener('click', function(){
        //     console.log('test');
        //     console.log(Datepicker.getDate());
        // });

        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function(){

                $(this).addClass('transition');
            }, function(){

                $(this).removeClass('transition');
            });


        });
    </script>



@endsection
