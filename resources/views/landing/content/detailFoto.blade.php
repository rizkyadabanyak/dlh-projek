@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px">

        <section class="wrapper">
            <div class="row">
                <div class="leftcolumn">
                    <div class="card shadow-card" style="">
                        <h2>{{$data->name}}</h2>
                        <h5>{{$data->created_at}}</h5>
                        <div class="row">
                            @foreach($galleries as $gallery)

                                <img src=" {{asset($gallery->img)}}" style="width: 100px; height: 100px" alt="..." class="img-thumbnail">
                            @endforeach
                            <div class="col-md-6">

                            </div>
                        </div>
                        <br>
                        <p style="color: var(--color-font);">{!! $data->desc !!}</p>


                    </div>

                </div>
                <div class="rightcolumn">
                    <div class="card shadow-card">
                        <a style="border-bottom: 6px #5ca863 solid;font-size: 20px;" class="text-orange">Cari Berita</a>
                        <form action="{{route('searchAction')}}" method="post" class="mb-5">
                            @csrf
                            <div class="d-flex justify-content-center ">
                                <div class="search"> <input type="text" class="typeahead search-input" placeholder="Search..." name="search"> <button class="search-icon"> <i class="fa fa-search"></i> </button> </div>
                            </div>
                        </form>
                    </div>
                    <div class="card shadow-card">
                        <a style="border-bottom: 6px #5ca863 solid;font-size: 20px;" class="text-orange">Berita Terpopuler</a>

                        <div class="row">
                            @forelse($news as $new)
                                <div class="col-12">
                                    <div class="card" style="border: 0px;padding-top: 2px;padding-bottom: 2px">
                                        <div class="card-horizontal">
                                            <div class="img-square-wrapper">
                                                <img class="img-card" src="{{asset($new->img)}}" alt="Card image cap" style="width: 100px;height: 100px;margin-top: 20px">
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('detail',$new->slug)}}" style="color: black">
                                                    <p class="card-title" style="font-family: 'Segoe UI';font-size: 10px">{{\Str::limit($new->name, 70, $end='...') }}</p>
                                                </a>
                                                <p class="card-text" style="font-family: 'Segoe UI';color: #5ca863;font-size: 10px">{{$new->category->name}}</p>
                                                <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);font-size: 10px">{!! \Str::limit($new->desc_banner, 30, $end='...') !!}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @empty
                                <h5 class="mt-5">belum ada berita</h5>
                            @endforelse
                        </div>
                    </div>
                    <div class="card shadow-card">
                        <a style="border-bottom: 6px #5ca863 solid;font-size: 20px;" class="text-orange">Agenda Terbaru</a>

                        <div class="row">
                            @forelse($agendas as $agenda)
                                <div class="col-12">
                                    <div class="card" style="border: 0px;padding-top: 2px;padding-bottom: 2px">
                                        <div class="card-horizontal">
                                            <div class="img-square-wrapper">
                                                <img class="img-card" src="{{asset($agenda->img)}}" alt="Card image cap" style="width: 100px;height: 100px;margin-top: 20px">
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('detail',$agenda->slug)}}" style="color: black">
                                                    <p class="card-title" style="font-family: 'Segoe UI';font-size: 10px">{{\Str::limit($agenda->name, 70, $end='...') }}</p>
                                                </a>
                                                <p class="card-text" style="font-family: 'Segoe UI';color: var(--color-scnd);font-size: 10px">{!! \Str::limit($agenda->desc_banner, 15, $end='...') !!}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @empty
                                <h5 class="mt-5">belum ada berita</h5>
                            @endforelse
                        </div>
                    </div>


                </div>
            </div>
            <div class="card shadow-card">


                <div class="row">
                    <div class="col-sm-5 col-md-6 col-12 pb-4">
                        <h1>Komentar</h1>
                        @forelse($comments as $comment)
                            <div class="comment mt-4 text-justify float-left" style="width: 100%"> <img src="https://i.imgur.com/yTFUilP.jpg" alt="" class="rounded-circle" width="40" height="40">
                                <h4>{{$comment->user_name}}</h4> <span>- {{\Carbon\Carbon::parse($comment->created_at)->toFormattedDateString()}}</span> <br>
                                <p>{{$comment->user_comment}}</p>
                            </div>
                        @empty
                            <h4>tidak ada comment</h4>
                        @endforelse

                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 offset-md-1 offset-sm-1 col-12 mt-4">
                        <form id="algin-form" method="post" action="{{route('commentNew',[$data->id,$cek])}}">
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <h4>Beri Komentar</h4> <label for="message">Komentar</label>
                                <textarea name="comment" id="" msg cols="30" rows="5" class="form-control" style="background-color: #ffffff;">

                                </textarea>
                            </div>
                            <div class="form-group"> <label for="name">Nama</label> <input type="text" name="name" id="fullname" class="form-control"> </div>
                            <div class="form-group"> <label for="email">Email</label> <input type="text" name="email" id="email" class="form-control"> </div>
                            <div class="form-group">
                                <button class="btn btn-success">Kirim</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
