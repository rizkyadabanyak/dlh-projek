@extends('landing.app')

@section('content')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */

            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px">
        <section class="wrapper">
            <div class="row">
                <div class="leftcolumn">
                    <div class="card shadow-card" style="padding: 30px">
                        <h5 class="bold"> Tanggal : {{\Carbon\Carbon::parse($date)->toFormattedDateString()}} </h5><br>
                        <div class="table-responsive-sm">
                            <table  id="example" class="table table-striped table-bordered" >
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Desc</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                    <th>File</th>

                                </tr>
                                </thead>
                                <tbody>
                                @forelse($agendas as $agenda)
                                <tr>
                                    <td><a href="{{route('detailAgenda',$agenda->slug)}}">{{$agenda->name}}</a></td>
                                    <td>{!! \Str::limit($agenda->desc_banner, 100, $end='......') !!}</td>
                                    <td>{{$agenda->location}}</td>
                                    <td class="{{($agenda->status == 'belum berlangsung') ? 'text-danger' : 'text-success'}}">{{$agenda->status}}</td>
                                    <td><a href="{{asset($agenda->file)}}"><button class="btn btn-info" {{($agenda->file == null || $agenda->file == '') ? 'disabled' : ''}}>Download</button></a> </td>

                                </tr>
                                @empty
                                    <tr>
                                        <td>Belum ada agenda pada tanggal {{$date}}</td>
                                    </tr>
                                @endforelse
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
                <div class="rightcolumn">

                    <div class="card shadow-card" style="width: 100%">
                        <div id="myCalendar" class="vanilla-calendar"></div>


                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{asset('datatabel/datatabel.js')}}"></script>
    <script>

        function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat)
            return [pad(d.getFullYear()), pad(d.getMonth()+1), d.getDate()].join('-')
        }

        let elem = document.querySelector('#myCalendar');

        let myCalendar = new VanillaCalendar({
            selector: "#myCalendar",
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            shortWeekday: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            onSelect: (data, elem) => {
                date = convertDate(data.date)
                console.log(convertDate(data.date))
                window.location = date;

            }
        })
        myCalendar.init();

        $(document).ready(function() {
            $('#example').DataTable();
        } );

    </script>

@endsection
