@extends('landing.app')

@section('content')
    @include('landing.components.style-search')

    <style>
        .card-horizontal {
            display: flex !important;
            flex: 1 1 auto !important;
        }
        * {
            box-sizing: border-box;
        }

        /* Add a gray background color with some padding */
        /* Header/Blog Title */
        .header {
            font-size: 40px;
            text-align: center;
            background: white;
        }

        /* Create two unequal columns that floats next to each other */
        /* Left column */
        .leftcolumn {
            float: left;
            width: 75%;
        }

        /* Right column */
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }

        /* Fake image */
        .fakeimg {
            background-color: #aaa;
            width: 100%;
            padding: 20px;
        }

        /* Add a card effect for articles */
        .card {
            border: 0px;
            background-color: var(--color-bg);
            padding: 20px;
            margin-top: 20px;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/

        }/* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
            margin-top: 20px;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 800px) {
            .leftcolumn, .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
        .shadow-card{
            box-shadow: rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }
    </style>
    <div class="header">
        <div style="height:300px;background-image: url('https://accessnsite.com/wp-content/uploads/2020/05/page-heading-background-contact-us.jpg');box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></div>
    </div>

    <div class="container py-5" style="max-width: 1200px;padding: 0px;">
        <div class="card shadow-card" style="width: 100%">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-12 pb-4">
                    <a style="border-bottom: 6px #5ca863 solid;font-size: 30px;" class="text-orange">E-PENGADUAN</a><br><br>
                    @include('admin.components.partials.message')
                    {{--                                <p>Fitur yang </p>--}}
                    <div class="row" style="padding: 10px">
                        <div class="col-sm-12 col-md-6 col-12 pb-4">
                            <img src="{{asset('assets/bumi.png')}}" class="img-fluid" alt="Responsive image">

                        </div>
                        <div class="col-sm-12 col-md-6 col- 12 pb-4" style="display: flex;align-items: center;justify-content: center;">
                            {{--                                        <p>Status Laporan Pengaduan</p><br>--}}
                            <div>
                                <h5>Status <a style="color: #00dd00">Laporan Pengaduan</a> </h5> <br>
                                <p>per tanggal {{\Carbon\Carbon::now()->toFormattedDateString()}}</p>
                                <div class="row" style="text-align: center;font-weight: bold">
                                    <div class="col-sm-12 col-md-4 col-12">
                                        <p>Masuk</p>
                                        <a class="text-info">{{$masuk}}</a>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-12">
                                        <p>Ditindaklanjuti</p>
                                        <a class="text-warning">{{$ditindaklanjuti}}</a>
                                    </div>
                                    <div class="col-sm-12 col-md-4 col-12">
                                        <p>Selesai</p>
                                        <a class="text-success">{{$selesai}}</a>
                                    </div>
                                </div>
                                <br>
                                <h3>
                                    Cara baru yang lebih praktis untuk melapor perusakan dan pencemaran lingkungan hidup
                                </h3>
                                <br><br>
                                <a href="{{route('pengaduan')}}" class="btn btn-success">Lapor !</a><br><br>
                                <label for="">Lacak Laporan Anda</label>
                                <form class="form-inline" method="POST" action="{{route('pengaduanActionCek')}}">
                                    @csrf
                                    <input type="text" name="code" id="code" class="form-control" placeholder="code..." style="width: 200px">
                                    <button class="btn btn-success ml-4 btn-submit" id="btn-submit">Lacak</button>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

    </script>

@endsection
