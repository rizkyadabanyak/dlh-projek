@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Related Websites</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
{{--                                @if($data == null)--}}
                                    <a href="{{route('admin.auth.relatedWebsites.create')}}" class="btn btn-success text-white">create</a>
{{--                                @endif--}}
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.relatedWebsites.index')}}",
                    },
                    columns: [
                        {
                            data: 'user',
                            name: 'user'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'img',
                            name: 'img'
                        },
                        {
                            data: 'url',
                            name: 'url'
                        },
                        {
                            data: 'active_flag',
                            name: 'active_flag'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
