@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dashboard</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Pengaduan</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                {{--                                <a href="{{route('admin.auth.agenda.create')}}" class="btn btn-success text-white">create</a>--}}

                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Telepon</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Informasi Detail</th>
                                            <th>Tujuan Informasi</th>
                                            <th>Cara Memperoleh Informasi</th>
                                            <th>Cara Mendapatkan Salinan Informasi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.permohonan.index')}}",
                    },
                    columns: [
                        {
                            data: 'client_name',
                            name: 'client_name'
                        },
                        {
                            data: 'client_email',
                            name: 'client_email'
                        },
                        {
                            data: 'client_phone',
                            name: 'client_phone'
                        },
                        {
                            data: 'client_date_of_birth',
                            name: 'client_date_of_birth'
                        },
                        {
                            data: 'client_detail_information',
                            name: 'client_detail_information'
                        },
                        {
                            data: 'client_target_information',
                            name: 'client_target_information'
                        },
                        {
                            data: 'client_memperoleh',
                            name: 'client_memperoleh'
                        },
                        {
                            data: 'client_mendapatkan',
                            name: 'client_mendapatkan'
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
