@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Log</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Log Client</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

{{--                                <a href="{{route('admin.auth.agenda.create')}}" class="btn btn-success text-white">create</a>--}}

                                <br><br>
                                @include('admin.components.partials.message')
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>Ip</th>
                                                <th>Browser</th>
                                                <th>Country</th>
                                                <th>Daerah</th>
                                                <th>Sistem Operasi</th>
                                                <th>path</th>
                                                <th>date</th>
                                                <th>time</th>
                                            </tr>
                                            </thead>

                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.logClients')}}",
                    },
                    columns: [

                        {
                            data: 'ip',
                            name: 'ip'
                        },
                        {
                            data: 'browser',
                            name: 'browser'
                        },
                        {
                            data: 'country',
                            name: 'country'
                        },
                        {
                            data: 'city',
                            name: 'city'
                        },
                        {
                            data: 'so',
                            name: 'so'
                        },
                        {
                            data: 'path',
                            name: 'path',
                        },
                        {
                            data: 'date',
                            name: 'date',
                        },
                        {
                            data: 'time',
                            name: 'time',
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
