@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>About Us</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Regulation/UU</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
{{--                                @if($data == null)--}}
                                    <a href="{{route('admin.auth.levels.create')}}" class="btn btn-success text-white">create</a>
{{--                                @endif--}}
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.levels.index')}}",
                    },
                    columns: [

                        {
                            data: 'level',
                            name: 'level'
                        },
                        {
                            data: 'active_flag',
                            name: 'active_flag'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },


                    ]
                });
            });
        </script>

    @endpush
@endsection
