@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>form Tupoksi</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.levels.update',$data->id) : route('admin.auth.levels.index')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if($data!=null)
                                        @method('put')
                                    @endif
                                        <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="level" id="name" class="form-control @error('level') is-invalid @enderror" value="{{($data!=null) ? $data->level : ''}}">
                                            @error('level')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
