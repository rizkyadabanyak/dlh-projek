@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dashboard</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">User</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <a href="{{route('admin.auth.users.create')}}" class="btn btn-success text-white">create</a>

                                <br><br>
                                @include('admin.components.partials.message')
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
{{--                                            <tbody>--}}
{{--                                                <tr></tr>--}}
{{--                                            </tbody>--}}

                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.users.index')}}",
                    },
                    columns: [

                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'role',
                            name: 'role'
                        },
                        {
                            data: 'active_flag',
                            name: 'active_flag'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
