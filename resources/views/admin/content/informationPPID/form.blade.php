@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Kategori</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.informationPPID.update',$data->id) : route('admin.auth.informationPPID.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    @if($data!=null)
                                        @method('put')
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama / Judul Informasi	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="title" id="name" class="form-control @error('title') is-invalid @enderror" value="{{($data!=null) ? $data->title : ''}}">
                                            @error('title')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ringkasan Isi Informasi	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="ringkasan" id="name" class="form-control @error('ringkasan') is-invalid @enderror" value="{{($data!=null) ? $data->ringkasan : ''}}">
                                            @error('ringkasan')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pejabat/Unit/Satuan Kerja yang Menguasai Informasi	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="pejabat" id="name" class="form-control @error('pejabat') is-invalid @enderror" value="{{($data!=null) ? $data->pejabat : ''}}">
                                            @error('pejabat')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Penanggung Jawab Pembuatan/Penerbitan Informasi	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="pj" id="pj" class="form-control @error('pj') is-invalid @enderror" value="{{($data!=null) ? $data->pj : ''}}">
                                            @error('pj')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Waktu dan Tempat Pembuatan Informasi	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="waktudantempat" id="name" class="form-control @error('waktudantempat') is-invalid @enderror" value="{{($data!=null) ? $data->waktudantempat : ''}}">
                                            @error('waktudantempat')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bentuk Informasi yang Tersedia	</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="bentuk" id="bentuk" class="form-control @error('bentuk') is-invalid @enderror" value="{{($data!=null) ? $data->bentuk : ''}}">
                                            @error('bentuk')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jangka Waktu Penyimpanan / Masa Retensi Arsip</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="jangka_waktu" id="jangka_waktu" class="form-control @error('jangka_waktu') is-invalid @enderror" value="{{($data!=null) ? $data->jangka_waktu : ''}}">
                                            @error('jangka_waktu')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
