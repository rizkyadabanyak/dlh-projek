@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Categories</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.informationPPID.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Nama / Judul Informasi</th>
                                            <th>Ringkasan Isi Informasi	</th>
                                            <th>Pejabat/Unit/Satuan Kerja yang Menguasai Informasi	</th>
                                            <th>Penanggung Jawab Pembuatan/Penerbitan Informasi	</th>
                                            <th>Waktu dan Tempat Pembuatan Informasi	</th>
                                            <th>Bentuk Informasi yang Tersedia	</th>
                                            <th>Jangka Waktu Penyimpanan / Masa Retensi Arsip</th>

                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.informationPPID.index')}}",
                    },
                    columns: [
                        {
                            data: 'title',
                            name: 'title'
                        },
                        {
                            data: 'ringkasan',
                            name: 'ringkasan'
                        },
                        {
                            data: 'pejabat',
                            name: 'pejabat'
                        },
                        {
                            data: 'pj',
                            name: 'pj'
                        },
                        {
                            data: 'waktudantempat',
                            name: 'waktudantempat'
                        },
                        {
                            data: 'bentuk',
                            name: 'bentuk'
                        },
                        {
                            data: 'jangka_waktu',
                            name: 'jangka_waktu'
                        },

                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush

@endsection
