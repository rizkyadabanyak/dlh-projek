@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">show New Galleries</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Gallery News {{$data->name}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="gallery gallery-md">
                                    @foreach($galleries as $gallery)
                                        <div class="gallery-item" data-image="{{asset($gallery->img)}}" data-title="{{$gallery->desc}}"></div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.newGalleries.index')}}",
                    },
                    columns: [
                        {
                            data: 'user',
                            name: 'user'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'desc',
                            name: 'desc'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush

@endsection
