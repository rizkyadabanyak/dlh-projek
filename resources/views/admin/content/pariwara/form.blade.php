@extends('admin.app')

@section('content')
    <style>
        .modal-backdrop.show{
            opacity: -0.5;
            position: unset;
        }
    </style>


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form Pariwara</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.pariwara.update',$data->id) : route('admin.auth.pariwara.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    @if($data!=null)
                                        @method('put')
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">
                                            @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Link</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="link" id="link" class="form-control @error('link') is-invalid @enderror" value="{{($data!=null) ? $data->link : ''}}">
                                            @error('link')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>
                                    @if($data!=null)
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select name="active_flag" class="form-control selectric" id="cars">
                                                    <option value="0" {{($data->active_flag == 0) ? 'selected' : ''}}>Non-Active</option>
                                                    <option value="1" {{($data->active_flag == 1) ? 'selected' : ''}}>Active</option>

                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
