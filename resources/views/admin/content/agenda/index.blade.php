@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Agenda</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <a href="{{route('admin.auth.agenda.create')}}" class="btn btn-success text-white">create</a>

                                <br><br>
                                @include('admin.components.partials.message')
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>User</th>
                                                <th>Name</th>
                                                <th>Desc</th>
                                                <th>Img</th>
                                                <th>Location</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Time</th>
                                                <th>Berjalan</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.agenda.index')}}",
                    },
                    columns: [
                        {
                            data: 'user',
                            name: 'user'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'desc',
                            name: 'desc'
                        },
                        {
                            data: 'img',
                            name: 'img'
                        },
                        {
                            data: 'location',
                            name: 'location'
                        },
                        {
                            data: 'date_start',
                            name: 'date_start'
                        },
                        {
                            data: 'date_end',
                            name: 'date_end'
                        },
                        {
                            data: 'time',
                            name: 'time'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'active_flag',
                            name: 'active_flag'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
