@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>form Tupoksi</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.regulations.update',$data->id) : route('admin.auth.regulations.index')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    @if($data!=null)
                                        @method('put')
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name  : ''}}">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomer</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="nomer" id="name" class="form-control @error('nomer') is-invalid @enderror" value="{{($data!=null) ? $data->nomer : ''}}">
                                            @error('nomer')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tahun</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="year" id="name" class="form-control @error('year') is-invalid @enderror" value="{{($data!=null) ? $data->year : ''}}">
                                            @error('year')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control" name="level_id">
                                                @if($data!=null)
                                                    <option value="{{$data->level_id}}" selected>{{$data->level->level}}</option>
                                                @endif
                                                @foreach($levels as $level)
                                                    <option value="{{$level->id}}">{{$level->level}}</option>
                                                @endforeach
                                            </select>
                                            @error('level_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">File</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="file" id="file">
                                            @error('file')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
