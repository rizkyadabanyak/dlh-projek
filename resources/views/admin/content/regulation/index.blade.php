@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>About Us</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Regulation/UU</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
{{--                                @if($data == null)--}}
                                    <a href="{{route('admin.auth.regulations.create')}}" class="btn btn-success text-white">create</a>
{{--                                @endif--}}
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Tingkat</th>
                                            <th>Status</th>
                                            <th>File</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.regulations.index')}}",
                    },
                    columns: [

                        {
                            data: 'name',
                            name: 'ip'
                        },
                        {
                            data: 'level_id',
                            name: 'level_id'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'file',
                            name: 'file'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },


                    ]
                });
            });
        </script>

    @endpush
@endsection
