@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dashboard</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Show Pengaduan</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

{{--                                <a href="{{route('admin.auth.agenda.create')}}" class="btn btn-success text-white">create</a>--}}
                                @include('admin.components.partials.message')

                                <br><br>
                                <table class="table-responsive">
                                    <tr>
                                        <td> <h4>Nama Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_name}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Email Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_email}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Tanggal lahir Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_date_of_birth}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Jenis Kelamin Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_gender}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Lokasi Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_location}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Alasan Mengadu </h4></td>
                                        <td> <h4> : {{$data->client_reason}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Dampak yang Dirasakan Pengadu </h4></td>
                                        <td> <h4> : {{$data->client_impact}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Pengaduan</h4></td>
                                        <td> <h4> : {{$data->client_complaint}}</h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>Status</h4></td>
                                        <td>
                                            @if($data->status == 0)
                                                <h4 class="text-danger"> : Belum Di tindak</h4>
                                            @elseif($data->status == 1)

                                                <h4 class="text-info"> : Sedang Di Tindak</h4>
                                            @else
                                                <h4 class="text-success"> : Selesai</h4>

                                            @endif
                                        </td>

                                    </tr>
                                    <tr>
                                        <td> <h4>File Pendukung</h4></td>
                                        <td> <h4> : <a href="{{asset($data->client_file)}}" class="btn btn-success">Lihat</a></h4></td>

                                    </tr>
                                    <tr>
                                        <td> <h4>KTP</h4></td>
                                        <td>
                                            <h4> : <a href="{{asset($data->ktp)}}" class="btn btn-success">Lihat</a></h4>
                                        </td>

                                    </tr>
                                </table>

                                @if($data->status == 0)
                                    <a href="{{route('admin.auth.complaintTindaklanjut',$data->id)}}" class="btn btn-info float-right" style="text-decoration: none;color: white">Tindak Lanjut</a>

                                @else
                                    <a href="{{route('admin.auth.complaintSelesai',$data->id)}}" class="btn btn-info float-right" style="text-decoration: none;color: white">Selesai</a>

                                @endif


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
