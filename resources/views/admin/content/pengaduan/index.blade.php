@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dashboard</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Pengaduan</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

{{--                                <a href="{{route('admin.auth.agenda.create')}}" class="btn btn-success text-white">create</a>--}}

                                <br><br>
                                @include('admin.components.partials.message')
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="item">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                <th>Tanggal Lahir</th>
                                                <th>Status</th>
                                                <th>Aduan</th>
                                                <th>Show</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.complaint.index')}}",
                    },
                    columns: [
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'client_name',
                            name: 'client_name'
                        },
                        {
                            data: 'client_email',
                            name: 'client_email'
                        },
                        {
                            data: 'client_phone',
                            name: 'client_phone'
                        },
                        {
                            data: 'client_date_of_birth',
                            name: 'client_date_of_birth'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'client_complaint',
                            name: 'client_complaint'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush
@endsection
