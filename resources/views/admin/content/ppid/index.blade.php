@extends('admin.app')

@section('content')


    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Master</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">PPID</h2>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.auth.ppid.create')}}" class="btn btn-success text-white">create</a>
                                <br><br>
                                @include('admin.components.partials.message')
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>user</th>
                                            <th>title</th>
                                            <th>img</th>
                                            <th>desc</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#item').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('admin.auth.ppid.index')}}",
                    },
                    columns: [
                        {
                            data: 'user',
                            name: 'user'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'img',
                            name: 'img'
                        },
                        {
                            data: 'desc',
                            name: 'desc'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>

    @endpush

@endsection
