<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
                <a href="index.html">Dlh</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Dlh</a>
        </div>
        @if(Auth::user()->role == 0)
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.dashboard')}}">Dashboard</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.users.index')}}">User</a></li>
                </ul>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.categories.index')}}">Kategori</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.news.index')}}">Berita</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.ppid.index')}}">PPID</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.newGalleries.index')}}">berita foto</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.agenda.index')}}">Agenda</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.carousel.index')}}">Carousel</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.pariwara.index')}}">Pariwara</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.relatedWebsites.index')}}">website terkait</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.report.index')}}">laporan</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.videoProfile.index')}}">Video Profile</a></li>

                </ul>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>About Us</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.auth.profile.index')}}">Profile</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.tupoksi.index')}}">Tupoksi</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.structure.index')}}">Struktur</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.regulations.index')}}">Peraturan/UU</a></li>
                    <li><a class="nav-link" href="{{route('admin.auth.levels.index')}}">TIngkat Peraturan/UU</a></li>
                </ul>
            </li>
            <li><a class="nav-link" href="{{route('admin.auth.logClients')}}"><i class="fas fa-pencil-ruler"></i> <span>Log Client</span></a></li>
            <li><a class="nav-link" href="{{route('admin.auth.complaint.index')}}"><i class="fas fa-pencil-ruler"></i> <span>Aduan</span></a></li>
            <li><a class="nav-link" href="{{route('admin.auth.permohonan.index')}}"><i class="fas fa-pencil-ruler"></i> <span>Permohonan Informasi PPID</span></a></li>
            <li><a class="nav-link" href="{{route('admin.auth.informationPPID.index')}}"><i class="fas fa-pencil-ruler"></i> <span>DAFTAR INFORMASI PUBLIK</span></a></li>

        </ul>
        @else
            <ul class="sidebar-menu">
                <li class="menu-header">Dashboard</li>
                <li class="nav-item dropdown active">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{route('admin.auth.dashboard')}}">Dashboard</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{route('admin.auth.categories.index')}}">Kategori</a></li>
                        <li><a class="nav-link" href="{{route('admin.auth.news.index')}}">Berita</a></li>
                        <li><a class="nav-link" href="{{route('admin.auth.ppid.index')}}">PPID</a></li>
                        <li><a class="nav-link" href="{{route('admin.auth.newGalleries.index')}}">berita foto</a></li>
{{--                        <li><a class="nav-link" href="{{route('admin.auth.agenda.index')}}">Agenda</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.carousel.index')}}">Carousel</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.pariwara.index')}}">Pariwara</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.relatedWebsites.index')}}">website terkait</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.report.index')}}">laporan</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.videoProfile.index')}}">Video Profile</a></li>--}}

                    </ul>
                </li>
{{--                <li class="nav-item dropdown">--}}
{{--                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>About Us</span></a>--}}
{{--                    <ul class="dropdown-menu">--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.profile.index')}}">Profile</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.tupoksi.index')}}">Tupoksi</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.structure.index')}}">Struktur</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.regulations.index')}}">Peraturan/UU</a></li>--}}
{{--                        <li><a class="nav-link" href="{{route('admin.auth.levels.index')}}">TIngkat Peraturan/UU</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li><a class="nav-link" href="{{route('admin.auth.logClients')}}"><i class="fas fa-pencil-ruler"></i> <span>Log Client</span></a></li>--}}
{{--                <li><a class="nav-link" href="{{route('admin.auth.complaint.index')}}"><i class="fas fa-pencil-ruler"></i> <span>Aduan</span></a></li>--}}
{{--                <li><a class="nav-link" href="{{route('admin.auth.permohonan.index')}}"><i class="fas fa-pencil-ruler"></i> <span>Permohonan Informasi PPID</span></a></li>--}}
{{--                <li><a class="nav-link" href="{{route('admin.auth.informationPPID.index')}}"><i class="fas fa-pencil-ruler"></i> <span>DAFTAR INFORMASI PUBLIK</span></a></li>--}}

            </ul>
        @endif
    </aside>
</div>
