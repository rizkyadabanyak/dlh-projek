<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2021 <div class="bullet"></div> Made with ♥ by<a href="https://nauval.in/">twentishcode</a>
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>
