
<!-- General CSS Files -->
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/bootstrap/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('asset-stisla/css/bootstrap4.3.min.css')}}">

<!-- Template CSS -->
<link rel="stylesheet" href="{{asset('asset-stisla/css/style.css')}}">
<link rel="stylesheet" href="{{asset('asset-stisla/css/components.css')}}">

<!-- CSS Libraries -->
<link rel="stylesheet" href="{{asset('asset-stisla/css/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('asset-stisla/css/dataTables.bootstrap4.min.css')}}">

<!-- include summernote css/js -->
<script src="{{asset('asset-stisla/js/jquery-3.5.1.min.js')}}"></script>
<link href="https://demo.getstisla.com/assets/modules/summernote/summernote-bs4.css" rel="stylesheet">

